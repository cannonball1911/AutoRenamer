﻿/* Solution: AutoRenamer, EpisodeRowValues.cs
 * Holds season number, episode number and episode url for each episode
 * Kai Sackl, 20.10.2020
 */

namespace AutoRenamer
{
    public class EpisodeRowValues
    {
        public int SeasonNumber { get; set; }
        public int EpisodeNumber { get; set; }
        public string EpisodeURL { get; set; }
    }
}
