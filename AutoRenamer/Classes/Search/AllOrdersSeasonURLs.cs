﻿using System.Collections.Generic;

/* Solution: AutoRenamer, AllOrdersSeasonURLs.cs
 * Holds lists of all orders available for a series
 * Kai Sackl, 20.10.2020
 */

namespace AutoRenamer
{
    /// <summary>
    /// Holds lists of all orders available for a series
    /// </summary>
    public class AllOrdersSeasonURLs
    {
        public List<OrderValues> Official { get; set; }
        public List<OrderValues> DVD { get; set; }
        public List<OrderValues> Absolute { get; set; }
        public List<OrderValues> Alternate { get; set; }
    }
}
