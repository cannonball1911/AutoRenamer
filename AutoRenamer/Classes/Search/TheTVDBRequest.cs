﻿using Newtonsoft.Json;
using System.Collections.Generic;

/* Solution: AutoRenamer, JsonRequestPayload.cs
 * Holds the json request payload for the thetvdb search request
 * Kai Sackl, 16.10.2020
 */

namespace AutoRenamer
{
    public class TheTVDBRequest
    {
        [JsonProperty("requests")]
        public List<RequestObject> Requests { get; set; }
    }

    public class RequestObject
    {
        [JsonProperty("indexName")]
        public string IndexName { get; set; }

        [JsonProperty("params")]
        public string Params { get; set; }
    }
}
