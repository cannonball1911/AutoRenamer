﻿/* Solution: AutoRenamer, EpisodeValues.cs
 * Holds season number, episode number, language type and episode title for one episode
 * Kai Sackl, 20.10.2020
 */

namespace AutoRenamer
{
    /// <summary>
    /// Holds season number, episode number, language type and episode title for one episode
    /// </summary>
    public class TranslatedEpisode
    {
        public int SeasonNumber { get; set; }
        public int EpisodeNumber { get; set; }
        public string LanguageType { get; set; }
        public string TranslatedEpisodeTitle { get; set; }
    }
}
