﻿/* Solution: AutoRenamer, OrderValues.cs
 * Holds season number and season url for each order
 * Kai Sackl, 20.10.2020
 */

namespace AutoRenamer
{
    public class OrderValues
    {
        public int SeasonNumber { get; set; }
        public string SeasonURL { get; set; }
    }
}
