﻿using System.Collections.Generic;

/* Solution: AutoRenamer, TheTVDBResponse.cs
 * Holds the json response data from TheTVDB.com
 * Kai Sackl, 16.10.2020
 */

namespace AutoRenamer
{
    public class TheTVDBResponse
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public List<string> Aliases { get; set; }
        public object Translations { get; set; }
        public string Image { get; set; }
    }
}
