﻿using System.Drawing;
using System.Windows.Forms;

/* Solution: AutoRenamer, DarkModeButton.cs
 * DarkModeComponent - Button
 * Kai Sackl, 06.11.2020
 */

namespace AutoRenamer.Classes.DarkMode
{
    public partial class DarkModeButton : Button
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle bounds = ClientRectangle;

            using (StringFormat stringFormat = new StringFormat())
            {
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                Color backColor;
                Color foreColor;

                if (Enabled)
                {
                    backColor = Globals.userSettings.DarkMode ? DarkModeColors.LightBlack : default;
                    foreColor = Globals.userSettings.DarkMode ? DarkModeColors.LightGray : default;
                }
                else
                {
                    backColor = Globals.userSettings.DarkMode ? DarkModeColors.DarkBlack : default;
                    foreColor = Globals.userSettings.DarkMode ? DarkModeColors.DarkGray : default;
                }

                using (SolidBrush backBrush = new SolidBrush(backColor))
                    e.Graphics.FillRectangle(backBrush, bounds);

                using (SolidBrush foreBrush = new SolidBrush(foreColor))
                    e.Graphics.DrawString(Text, Font, foreBrush, bounds, stringFormat);

                base.OnPaint(e);

                Color borderColor = Globals.userSettings.DarkMode ? DarkModeColors.DarkBlack : Color.Gray;
                ControlPaint.DrawBorder3D(e.Graphics, bounds, Border3DStyle.RaisedInner);
                ControlPaint.DrawBorder(e.Graphics, bounds, borderColor, 2, ButtonBorderStyle.Outset, borderColor, 2, ButtonBorderStyle.Outset, borderColor, 2, ButtonBorderStyle.Outset, borderColor, 2, ButtonBorderStyle.Outset);
            }
        }
    }
}
