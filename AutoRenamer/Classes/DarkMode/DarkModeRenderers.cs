﻿using System.Drawing;
using System.Windows.Forms;

/* Solution: AutoRenamer, DarkModeRenderers.cs
 * Holds the overwritten renderers for dark mode
 * Kai Sackl, 30.10.2020
 */

namespace AutoRenamer.Classes.DarkMode
{
    /// <summary>
    /// Holds the overwritten renderers for controls for dark mode
    /// </summary>
    internal class DarkModeRenderers
    {
        /// <summary>
        /// Dark mode - MenuStrip renderer
        /// </summary>
        internal class MenuStripRenderer : ToolStripProfessionalRenderer
        {
            protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
            {
                Rectangle rectangle = new Rectangle(Point.Empty, e.Item.Size);
                Color color = e.Item.Selected ? DarkModeColors.LightBlack : DarkModeColors.DarkBlack;
                using (SolidBrush brush = new SolidBrush(color))
                    e.Graphics.FillRectangle(brush, rectangle);
            }
        }
    }
}
