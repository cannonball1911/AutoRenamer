﻿using System.Drawing;

/* Solution: AutoRenamer, Colors.cs
 * Holds the colors for dark mode
 * Kai Sackl, 30.10.2020
 */

namespace AutoRenamer.Classes.DarkMode
{
    /// <summary>
    /// Holds the colors for dark mode
    /// </summary>
    internal static class DarkModeColors
    {
        internal static Color DarkBlack
        {
            get { return Color.FromArgb(30, 30, 30); }
        }
        internal static Color LightBlack
        {
            get { return Color.FromArgb(45, 45, 45); }
        }
        internal static Color LightGray
        {
            get { return Color.FromArgb(200, 200, 200); }
        }
        internal static Color DarkGray
        {
            get { return Color.FromArgb(60, 60, 60); }
        }
        internal static Color ButtonSuccessDarkGreen
        {
            get { return Color.FromArgb(0, 60, 0); }
        }
        internal static Color WaterMarkActiveForeColor
        {
            get { return Color.FromArgb(130, 130, 130); }
        }
        internal static Color WaterMarkForeColor
        {
            get { return Color.FromArgb(95, 95, 95); }
        }
    }
}
