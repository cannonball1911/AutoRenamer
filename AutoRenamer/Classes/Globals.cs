﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using AutoRenamer.Classes.DarkMode;

/* AutoRenamer, Globals.cs
 * Holds global variables for the solution
 * Kai Sackl, 21.10.2020
 */

namespace AutoRenamer
{
    /// <summary>
    /// Holds global variables for the solution
    /// </summary>
    internal static class Globals
    {
        internal readonly static string settingsFilePath = @"UserSettings.xml";
        internal readonly static string noEpisodeInfoOrTitleAvailableText = @"*No episode information available / No title available for selected languages*";
        internal readonly static string loadMetadataText = "Lade Metadaten...\nInfo: Das laden der Metadaten kann - je nach Episodenanzahl einer Serie - mehrere Minuten dauern!";

        #region UserSettings
        /// <summary>
        /// User settings instance
        /// </summary>
        internal static Settings userSettings = new Settings();

        /// <summary>
        /// Creates the xml settings file, if file not exists
        /// else writes to the xml settings file
        /// </summary>
        internal static void WriteSettingsToFile(string settingsFilePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            using (StreamWriter streamWriter = new StreamWriter(settingsFilePath))
                xmlSerializer.Serialize(streamWriter, userSettings);
        }

        /// <summary>
        /// Load user settings from file
        /// </summary>
        internal static void LoadSettingsFromFile(string settingsFilePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            using (FileStream fileStream = new FileStream(settingsFilePath, FileMode.Open))
                userSettings = (Settings)xmlSerializer.Deserialize(fileStream);
        }
        #endregion

        #region ChangeUITheme
        /// <summary>
        /// Change the UI theme of the given form
        /// </summary>
        /// <param name="form">Form (this) where the UI theme will be changed</param>
        /// <param name="darkMode">Change to dark mode</param>
        internal static void ChangeToDarkMode(Form form, bool darkMode)
        {
            Control.ControlCollection controlCollection = form.Controls;

            Color darkBlack = darkMode ? DarkModeColors.DarkBlack : default;
            Color lightBlack = darkMode ? DarkModeColors.LightBlack : default;
            Color lightGray = darkMode ? DarkModeColors.LightGray : default;
            ToolStripProfessionalRenderer menuStripRenderer = 
                darkMode ? new DarkModeRenderers.MenuStripRenderer() : default;

            form.BackColor = lightBlack;

            foreach (Control allControls in controlCollection)
            {
                if (allControls is Button)
                {
                    allControls.BackColor = lightBlack;
                    allControls.ForeColor = lightGray;
                }
                else if (allControls is GroupBox)
                {
                    allControls.BackColor = lightBlack;
                    allControls.ForeColor = lightGray;

                    foreach (Control groupBoxControl in allControls.Controls)
                    {
                        if (groupBoxControl is Button button)
                        {
                            groupBoxControl.BackColor = lightBlack;
                            groupBoxControl.ForeColor = lightGray;
                            if (!darkMode)
                            {
                                button.BackColor = SystemColors.ButtonFace;
                                button.UseVisualStyleBackColor = true;
                            }
                        }
                        else if (groupBoxControl is ListBox || groupBoxControl is TextBox || groupBoxControl is NumericUpDown || groupBoxControl is ComboBox)
                        {
                            groupBoxControl.BackColor = darkBlack;
                            groupBoxControl.ForeColor = lightGray;
                        }
                        
                        // Change watermark (active) foreColor if textbox is watermarkedTextBox
                        if (groupBoxControl is KaisLib.Controls.WatermarkedTextBox watermarkedTextBox)
                        {
                            if (darkMode)
                            {
                                watermarkedTextBox.WaterMarkActiveForeColor = DarkModeColors.WaterMarkActiveForeColor;
                                watermarkedTextBox.WaterMarkForeColor = DarkModeColors.WaterMarkForeColor;
                            }
                            else
                            {
                                watermarkedTextBox.WaterMarkActiveForeColor = Color.Gray;
                                watermarkedTextBox.WaterMarkForeColor = Color.LightGray;
                            }
                        }

                    }
                }
                else if (allControls is MenuStrip menuStrip)
                {
                    allControls.BackColor = darkBlack;
                    allControls.ForeColor = lightGray;
                    menuStrip.Renderer = menuStripRenderer;

                    foreach (ToolStripMenuItem toolStripMenuItem in menuStrip.Items)
                    {
                        foreach (ToolStripDropDownItem toolStripDropDownItem in toolStripMenuItem.DropDownItems)
                        {
                            toolStripDropDownItem.BackColor = lightBlack;
                            toolStripDropDownItem.ForeColor = lightGray;
                        }
                    }
                }
                else if (allControls is TabControl)
                {
                    foreach (Control tabControl in allControls.Controls)
                    {
                        if (tabControl is TabPage)
                        {
                            tabControl.BackColor = lightBlack;
                            tabControl.ForeColor = lightGray;

                            foreach (Control tabSubControl in tabControl.Controls)
                            {
                                if (tabSubControl is GroupBox)
                                {
                                    tabSubControl.BackColor = lightBlack;
                                    tabSubControl.ForeColor = lightGray;

                                    foreach (Control groupBoxControl in tabSubControl.Controls)
                                    {
                                        if (groupBoxControl is ComboBox)
                                        {
                                            groupBoxControl.BackColor = darkBlack;
                                            groupBoxControl.ForeColor = lightGray;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (allControls is TableLayoutPanel)
                {
                    allControls.BackColor = lightBlack;
                    allControls.ForeColor = lightGray;
                    foreach (Control aboutBoxControl in allControls.Controls)
                    {
                        if (aboutBoxControl is TextBox)
                        {
                            aboutBoxControl.BackColor = darkBlack;
                            aboutBoxControl.ForeColor = lightGray;
                        }
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Sets the passed button to default color
        /// </summary>
        /// <param name="button">Passed button</param>
        internal static void SetButtonDefaultColor(Button button)
        {
            if (userSettings.DarkMode)
            {
                button.BackColor = DarkModeColors.LightBlack;
                button.ForeColor = DarkModeColors.LightGray;
                button.UseVisualStyleBackColor = false;
                return;
            }
            button.BackColor = SystemColors.ButtonFace;
            button.ForeColor = default;
            button.UseVisualStyleBackColor = true;
        }
    }
}
