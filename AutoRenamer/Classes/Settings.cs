﻿using System;

/* Solution: AutoRenamer, Settings.cs
 * Contains the user settings
 * Kai Sackl, 22.10.2020
 */

namespace AutoRenamer
{
    [Serializable]
    public class Settings
    {
        public string MetadataFirstLanguage { get; set; }
        public string MetadataFallbackLanguage { get; set; }
        public bool AddEpisodenameToFilename { get; set; }
        public bool AddEpisodenameToFileDetails { get; set; }
        public bool DarkMode { get; set; }

        public Settings()
        {
            MetadataFirstLanguage = "eng";
            MetadataFallbackLanguage = "eng";
            AddEpisodenameToFilename = true;
            AddEpisodenameToFileDetails = false;
            DarkMode = false;
        }
    }
}

    
