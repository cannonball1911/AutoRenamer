﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoRenamer.Classes.DarkMode;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/* Solution: AutoRenamer, InitWindow.cs
 * Rename multiple video files so that Plex can retrieve the required information from the database
 * Kai Sackl, 07.08.2018
 */

namespace AutoRenamer
{
    public partial class InitWindow : Form
    {
        #region Private Globals
        /// <summary>
        /// String of original filepath (e.g.: C:\Videos\)
        /// </summary>
        private string originalFilePath;
        /// <summary>
        /// Array of original filenames (e.g.: example.mkv)
        /// </summary>
        private string[] originalFileNames;
        private static readonly HttpClient httpClient = new HttpClient();
        private List<TheTVDBResponse> fetchedDataList; // TheTVDB.com fetched data list
        /// <summary>
        /// Contains the lists of every fetched order of a series
        /// </summary>
        private AllOrdersSeasonURLs allOrdersSeasonURLs;
        private readonly string theTVDBWebsiteURL = @"https://thetvdb.com";
        /// <summary>
        /// Contains the season number, episode number and url to every episode (of every season) of a series for one order (Official, DVD, Absolute or Alternate)
        /// </summary>
        private List<EpisodeRowValues> episodeRowValuesForAllSeasonsOnOrder;
        private List<List<TranslatedEpisode>> translatedEpisodeTitlesForAllSeasons;
        private int lastSeletedIndexInListbox;
        private List<TranslatedEpisode> translatedEpisodeTitlesForOneSeason;
        private static readonly HtmlWeb webClientHtmlAgilityPack = new HtmlWeb();
        #endregion

        public InitWindow()
        {
            InitializeComponent();
            Init();
        }

        #region Button Events
        private void BtnFileSelection_Click(object sender, EventArgs e)
        {
            Clear();
            using (OpenFileDialog ofd = new OpenFileDialog { Multiselect = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    BtnFileSelection.BackColor = Globals.userSettings.DarkMode ? DarkModeColors.ButtonSuccessDarkGreen : Color.LightGreen;
                    GetFileNamesAndAddToList(ofd.FileNames);
                    SetSeriesNameAndSeasonNumber(ofd.FileNames);
                }
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(originalFilePath)) { MessageBox.Show("Keine Dateien ausgewählt!", "Task failed successfully!"); return; }
            if (string.IsNullOrWhiteSpace(TbSeriesName.Text)) { MessageBox.Show("Kein Serienname angebeben!", "Task failed successfully!"); return; }

            string seriesName = TbSeriesName.Text.Trim();
            int episodeNumber = Convert.ToInt32(NumFirstEpisodeNumber.Value);
            int seasonNumber = Convert.ToInt32(NumSeasonNumber.Value);

            string seasonText = seasonNumber < 10 ? $"S0{seasonNumber}" : $"S{seasonNumber}";

            foreach (string originalFileName in originalFileNames)
            {
                string episodeText = episodeNumber < 10 ? $"E0{episodeNumber}" : $"E{episodeNumber}";
                string fileExtension = Path.GetExtension(originalFileName);
                string newFileName = $"{seriesName} - {seasonText}{episodeText}{fileExtension}";
                string newFilePath = "";
                const int maxFilepathLengthLimit = 247;
                string filepathTooLongMessage = $"Der neue Dateipfad übersteigt das Limit von <= {maxFilepathLengthLimit} zeichen! (Danke, Microsoft!)\n" +
                                                $"Verschieben Sie die Datei in eine kürzere Ordnerstruktur und versuchen Sie es erneut.";
                TranslatedEpisode actualTranslatedEpisode = null;

                if (Globals.userSettings.AddEpisodenameToFilename)
                {
                    actualTranslatedEpisode = translatedEpisodeTitlesForOneSeason.Find(x => x.EpisodeNumber == episodeNumber);
                    if (actualTranslatedEpisode != null)
                    {
                        if (actualTranslatedEpisode.TranslatedEpisodeTitle != Globals.noEpisodeInfoOrTitleAvailableText)
                        {
                            string newFilePathWithoutEpisodename = Path.Combine(originalFilePath, newFileName);
                            newFileName = $"{seriesName} - {seasonText}{episodeText} - {actualTranslatedEpisode.TranslatedEpisodeTitle.Trim()}{fileExtension}";
                            string newFilePathWithEpisodename = Path.Combine(originalFilePath, newFileName);
                            if (IsFilePathToLong(newFilePathWithEpisodename))
                            {
                                int tooLongCount = newFilePathWithEpisodename.Length - maxFilepathLengthLimit;
                                int episodeNameLength = actualTranslatedEpisode.TranslatedEpisodeTitle.Length;

                                if (episodeNameLength > tooLongCount)
                                {
                                    string shortenedEpisodename = actualTranslatedEpisode.TranslatedEpisodeTitle.Substring(0, episodeNameLength - tooLongCount - 1).Trim();
                                    string shortenedNewFileName = $"{seriesName} - {seasonText}{episodeText} - {shortenedEpisodename}{fileExtension}";
                                    string shortenedNewFilePath = Path.Combine(originalFilePath, shortenedNewFileName);
                                    newFilePath = shortenedNewFilePath;
                                }
                                else
                                {
                                    MessageBox.Show(filepathTooLongMessage, "Dateipfad zu lang!");
                                    return;
                                }
                            }
                            else
                                newFilePath = newFilePathWithEpisodename;
                        }
                        else
                            newFilePath = Path.Combine(originalFilePath, newFileName);
                    }
                    else
                        newFilePath = Path.Combine(originalFilePath, newFileName);
                }
                else
                    newFilePath = Path.Combine(originalFilePath, newFileName);

                if (IsFilePathToLong(newFilePath))
                {
                    MessageBox.Show(filepathTooLongMessage, "Dateipfad zu lang!");
                    return;
                }
                if (newFileName.Any(Path.GetInvalidFileNameChars().Contains)) { MessageBox.Show(newFileName, "Ungültiger neuer Dateiname!"); return; }

                try
                {
                    System.IO.File.Move(Path.Combine(originalFilePath, originalFileName), newFilePath);

                    if (Globals.userSettings.AddEpisodenameToFileDetails)
                    {
                        TagLib.File tagLibFile = TagLib.File.Create(newFilePath);

                        if (actualTranslatedEpisode != null)
                            tagLibFile.Tag.Title = $"{seriesName} - {seasonText}{episodeText} - {actualTranslatedEpisode.TranslatedEpisodeTitle.Trim()}";
                        else
                            tagLibFile.Tag.Title = $"{seriesName} - {seasonText}{episodeText}";

                        tagLibFile.Save();
                    }
                }
                catch (IOException ex) { MessageBox.Show($"{ex.Message} ({newFilePath})\n\nStacktrace:{ex.StackTrace}", "Task failed successfully!"); return; }
                catch (Exception ex) { MessageBox.Show($"{ex.Message}\n\nStacktrace:{ex.StackTrace}", "Task failed successfully!"); return; }

                episodeNumber++;
            }
            Clear();
        }
        #endregion

        #region DragAndDrop Events
        private void LbAuswahl_DragEnter(object sender, DragEventArgs e)
        {
            string[] fullFileNames = (string[])e.Data.GetData(DataFormats.FileDrop);

            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) && !HasFolder(fullFileNames)
                ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void LbAuswahl_DragDrop(object sender, DragEventArgs e)
        {
            Clear();
            string[] fullFileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
            GetFileNamesAndAddToList(fullFileNames);
            SetSeriesNameAndSeasonNumber(fullFileNames);
            if (!string.IsNullOrWhiteSpace(originalFilePath))
                BtnFileSelection.BackColor = Globals.userSettings.DarkMode ? DarkModeColors.ButtonSuccessDarkGreen : Color.LightGreen;
        }
        #endregion

        #region MenuStrip Events
        private void MstItemFileSelection_Click(object sender, EventArgs e) => BtnFileSelection.PerformClick();

        private void MstItemFolderCreate_Click(object sender, EventArgs e)
        {
            CreateFolderWindow createFolder = new CreateFolderWindow();
            createFolder.Show();
        }

        private void MstItemHelpAbout_Click(object sender, EventArgs e)
        {
            using (AboutBox about = new AboutBox())
                about.ShowDialog();
        }

        private void MstItemOptionsSettings_Click(object sender, EventArgs e)
        {
            using (SettingsWindow settingsWindow = new SettingsWindow())
            {
                settingsWindow.ShowDialog();
                NumSeasonNumber_ValueChanged(NumSeasonNumber, EventArgs.Empty);
                AddRetrievedJsonSearchDataToListbox(fetchedDataList, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
                LbSeriesOnTheTVDB.SelectedIndex = lastSeletedIndexInListbox;
            }
        }
        #endregion

        #region TextboxEvents
        private void TbSeriesName_TextChanged(object sender, EventArgs e)
        {
            if (TbSeriesName.Text.Length > 0 && !string.IsNullOrWhiteSpace(TbSeriesName.Text))
                BtnSearchOnTheTVDB.Enabled = true;
            else
                BtnSearchOnTheTVDB.Enabled = false;
        }
        #endregion

        #region PictureBox Events
        private void PbxThumbnail_DoubleClick(object sender, EventArgs e)
        {
            if (PbxThumbnail.Image != null)
            {
                string tempSavePath = Path.Combine(Path.GetTempPath(), $"autorenamer_{Guid.NewGuid()}.jpg");
                PbxThumbnail.Image.Save(tempSavePath, ImageFormat.Jpeg);
                Process.Start(tempSavePath);
            }
        }
        #endregion

        #region Methods
        private void Init()
        {
            SetVersionNumberAndPlatform(ProductVersion);

            if (System.IO.File.Exists(Globals.settingsFilePath))
                Globals.LoadSettingsFromFile(Globals.settingsFilePath);
            else
                Globals.WriteSettingsToFile(Globals.settingsFilePath);

            ToolTip firstEpisodeToolTip = new ToolTip
            {
                IsBalloon = true,
                ShowAlways = true,
                AutoPopDelay = 1000 * 20
            };

            firstEpisodeToolTip.SetToolTip(NumFirstEpisodeNumber,
                "Nummerierung, welche die erste Episode der eingefügten Dateien beim auto. umbenennen haben soll. (E01, E12, usw.)");

            fetchedDataList = new List<TheTVDBResponse>();
            allOrdersSeasonURLs = new AllOrdersSeasonURLs()
            {
                Official = new List<OrderValues>(),
                DVD = new List<OrderValues>(),
                Absolute = new List<OrderValues>(),
                Alternate = new List<OrderValues>()
            };
            episodeRowValuesForAllSeasonsOnOrder = new List<EpisodeRowValues>();
            translatedEpisodeTitlesForAllSeasons = new List<List<TranslatedEpisode>>();
            lastSeletedIndexInListbox = -1;
            translatedEpisodeTitlesForOneSeason = new List<TranslatedEpisode>();

            if (Globals.userSettings.DarkMode)
                Globals.ChangeToDarkMode(this, true);
            else
                Globals.ChangeToDarkMode(this, false);
        }

        /// <summary>
        /// Gets the filename of each fullFileName from the passed array,
        /// and push them in a new array and sort them alphabetically
        /// Gets once the original filepath and set it
        /// </summary>
        /// <param name="stringArray">Array with fullFileNames</param>
        private void GetFileNamesAndAddToList(string[] stringArray)
        {
            originalFilePath = Path.GetDirectoryName(stringArray[0]);
            originalFileNames = new string[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++)
                originalFileNames[i] = Path.GetFileName(stringArray[i]);

            Array.Sort(originalFileNames, new StrCmpLogicalComparer());

            foreach (string filename in originalFileNames)
                LbSelection.Items.Add(filename);

            LblFileCounter.Text = $"Datei(en): {stringArray.Length}";
        }

        /// <summary>
        /// Clears the original filepath, clears the listbox,
        /// sets the button (BtnFileSelection) to default color
        /// and clear the filecounter
        /// </summary>
        private void Clear()
        {
            originalFilePath = "";
            LblFileCounter.Text = "";
            LbSelection.Items.Clear();
            Globals.SetButtonDefaultColor(BtnFileSelection);
        }

        /// <summary>
        /// Checks if array of fullFileNames contains folder(s)
        /// </summary>
        /// <param name="stringArray">Array of fullFileNames</param>
        /// <returns>true, if fullFileNames contains folder(s)</returns>
        private bool HasFolder(string[] stringArray)
        {
            foreach (string fullFileName in stringArray)
                if (System.IO.File.GetAttributes(fullFileName).HasFlag(FileAttributes.Directory))
                    return true;
            return false;
        }

        /// <summary>
        /// Gets the season number and the series name and sets it
        /// if the files are already in the correct folders
        /// eg.: "...\SeriesName\Season XX\example.mp4"
        /// </summary>
        /// <param name="fullFileNames"></param>
        private void SetSeriesNameAndSeasonNumber(string[] fullFileNames)
        {
            string[] splittedPath = fullFileNames[0].Split(Path.DirectorySeparatorChar).Reverse().ToArray();

            if (splittedPath[1].ToLower().Contains("season"))
            {
                string[] splittedSeasonName = splittedPath[1].Split(' ');
                NumSeasonNumber.Value = Convert.ToInt32(splittedSeasonName[1]);
                TbSeriesName.Text = splittedPath[2].Trim();
            }
        }

        /// <summary>
        /// Replaces every invalid char with an underscore ('_')
        /// </summary>
        /// <param name="episodename"></param>
        /// <returns></returns>
        private string RemoveInvalidChars(string episodename)
        {
            return string.Concat(episodename.Split(Path.GetInvalidFileNameChars()));
        }

        /// <summary>
        /// Checks if a filepath exceeds the limit of 247 chars
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private bool IsFilePathToLong(string filepath)
        {
            const int maxCharacters = 247;
            return filepath.Length > maxCharacters;
        }

        /// <summary>
        /// Set version number and platform of the tool
        /// </summary>
        /// <param name="version"></param>
        private void SetVersionNumberAndPlatform(string version)
        {
            string platform = Environment.Is64BitProcess ? "x64" : "x32";
            Text = $"AutoRenamer v{version} ({platform})";
        }
        #endregion

        #region TheTVDB - Clear Methods
        /// <summary>
        /// Clears the TheTVDB search items
        /// List, Listbox & Picturebox
        /// </summary>
        private void ClearTheTVDBSearch()
        {
            fetchedDataList.Clear();
            LbSeriesOnTheTVDB.Items.Clear();
            PbxThumbnail.Image = null;
            ClearOrderListsEpisodeListsAndCombobox();
            ClearSeasonInfo();
            BtnGetMetadata.Enabled = false;
            lastSeletedIndexInListbox = -1;
            CobOrder.Enabled = false;
            translatedEpisodeTitlesForOneSeason.Clear();
            LblEpisodeCount.Text = "";
            LblEpisodeCountCounter.Text = "";
        }

        /// <summary>
        /// Clears the order lists and order combo box
        /// </summary>
        private void ClearOrderListsEpisodeListsAndCombobox()
        {
            allOrdersSeasonURLs.Official.Clear();
            allOrdersSeasonURLs.DVD.Clear();
            allOrdersSeasonURLs.Absolute.Clear();
            allOrdersSeasonURLs.Alternate.Clear();
            CobOrder.Items.Clear();
            CobOrder.Text = "";
            episodeRowValuesForAllSeasonsOnOrder.Clear();
            LbEpisodeNames.Items.Clear();
            translatedEpisodeTitlesForAllSeasons.Clear();
        }

        /// <summary>
        /// Clears the episode filename listbox, episode row values list and translated episode titles listbox
        /// </summary>
        private void ClearSeasonInfo()
        {
            episodeRowValuesForAllSeasonsOnOrder.Clear();
            LbEpisodeNames.Items.Clear();
            translatedEpisodeTitlesForAllSeasons.Clear();
        }

        /// <summary>
        /// Clears SeasonInfo stuff, clears EpisodeCount
        /// </summary>
        private void ClearOnGetMetadataClick()
        {
            ClearSeasonInfo();
            ClearEpisodeCount();
        }

        /// <summary>
        /// Clears episode count labels
        /// </summary>
        private void ClearEpisodeCount()
        {
            LblEpisodeCount.Text = "";
            LblEpisodeCountCounter.Text = "";
        }
        #endregion

        #region TheTVDB - Helper Methods
        /// <summary>
        /// Selects the first item in the listbox
        /// </summary>
        private void SetListboxFirstIndex()
        {
            if (LbSeriesOnTheTVDB.Items.Count > 0)
                LbSeriesOnTheTVDB.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets the selected index of the listbox
        /// </summary>
        /// <param name="index"></param>
        private void SetListboxLastIndexIfAvailable(int index)
        {
            if (index < LbSeriesOnTheTVDB.Items.Count)
                LbSeriesOnTheTVDB.SelectedIndex = index;
        }

        /// <summary>
        /// Lock controls on search button click
        /// </summary>
        private void LockControlsOnSearch(bool lockControl)
        {
            BtnSearchOnTheTVDB.Enabled = !lockControl;
            NumSeasonNumber.Enabled = !lockControl;
            BtnClearSearch.Enabled = !lockControl;
            CobOrder.Enabled = !lockControl;
            BtnStart.Enabled = !lockControl;
            BtnGetMetadata.Enabled = !lockControl;
        }
        #endregion

        #region TheTVDB - Button Events
        private void LbFetchFromTheTVDB_DoubleClick(object sender, EventArgs e)
        {
            // On double-click, open up series entry in browser
            if (LbSeriesOnTheTVDB.Items.Count > 0 && LbSeriesOnTheTVDB.SelectedIndex >= 0)
                try { Process.Start(fetchedDataList[LbSeriesOnTheTVDB.SelectedIndex].Url); }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }
        }

        private async void BtnSearchOnTheTVDB_Click(object sender, EventArgs e)
        {
            ClearTheTVDBSearch();
            LockControlsOnSearch(true);
            await SendJsonSearchRequest();
            LockControlsOnSearch(false);

            if (fetchedDataList.Count > 0)
            {
                AddRetrievedJsonSearchDataToListbox(fetchedDataList, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
                SetListboxFirstIndex();
            }
        }

        private void BtnClearSearch_Click(object sender, EventArgs e)
        {
            ClearTheTVDBSearch();
            BtnGetMetadata.Enabled = false;
            BtnClearSearch.Enabled = false;
        }

        private void BtnGetMetadata_Click(object sender, EventArgs e)
        {
            LockControlsOnSearch(true);
            ClearOnGetMetadataClick();

            if ((string)CobOrder.SelectedItem == "Official (Aired) Order" && allOrdersSeasonURLs.Official.Count > 0)
            {
                LblLoadMetadataInfo.Text = Globals.loadMetadataText;
                GetSeasonInfoForOrderAndAddToList(allOrdersSeasonURLs.Official);
                GetEpisodeInfoAndAddToList(episodeRowValuesForAllSeasonsOnOrder);
                AddEpisodeNamesToListbox(translatedEpisodeTitlesForAllSeasons, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
            }
            else if ((string)CobOrder.SelectedItem == "DVD Order" && allOrdersSeasonURLs.DVD.Count > 0)
            {
                LblLoadMetadataInfo.Text = Globals.loadMetadataText;
                GetSeasonInfoForOrderAndAddToList(allOrdersSeasonURLs.DVD);
                GetEpisodeInfoAndAddToList(episodeRowValuesForAllSeasonsOnOrder);
                AddEpisodeNamesToListbox(translatedEpisodeTitlesForAllSeasons, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
            }
            else if ((string)CobOrder.SelectedItem == "Absolute Order" && allOrdersSeasonURLs.Absolute.Count > 0)
            {
                LblLoadMetadataInfo.Text = Globals.loadMetadataText;
                GetSeasonInfoForOrderAndAddToList(allOrdersSeasonURLs.Absolute);
                GetEpisodeInfoAndAddToList(episodeRowValuesForAllSeasonsOnOrder);
                AddEpisodeNamesToListbox(translatedEpisodeTitlesForAllSeasons, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
            }
            else if ((string)CobOrder.SelectedItem == "Alternate (Director's Cut) Order" && allOrdersSeasonURLs.Alternate.Count > 0)
            {
                LblLoadMetadataInfo.Text = Globals.loadMetadataText;
                GetSeasonInfoForOrderAndAddToList(allOrdersSeasonURLs.Alternate);
                GetEpisodeInfoAndAddToList(episodeRowValuesForAllSeasonsOnOrder);
                AddEpisodeNamesToListbox(translatedEpisodeTitlesForAllSeasons, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
            }

            LockControlsOnSearch(false);
        }
        #endregion

        #region TheTVDB - Listbox Events
        private void LbFetchFromTheTVDB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lastSeletedIndexInListbox != LbSeriesOnTheTVDB.SelectedIndex)
            {
                LockControlsOnSearch(true);
                ClearOrderListsEpisodeListsAndCombobox();
                ClearEpisodeCount();
                GetSeasonURLsForAllOrdersAndAddToList(fetchedDataList[LbSeriesOnTheTVDB.SelectedIndex].Url);
                PbxThumbnail.LoadAsync(fetchedDataList[LbSeriesOnTheTVDB.SelectedIndex].Image);
                LockControlsOnSearch(false);
            }
        }
        #endregion

        #region TheTVDB - NumericUpDown Events
        private void NumSeasonNumber_ValueChanged(object sender, EventArgs e)
        {
            ClearEpisodeCount();
            LbEpisodeNames.Items.Clear();
            translatedEpisodeTitlesForOneSeason.Clear();
            if (translatedEpisodeTitlesForAllSeasons.Count > 0)
                AddEpisodeNamesToListbox(translatedEpisodeTitlesForAllSeasons, Globals.userSettings.MetadataFirstLanguage, Globals.userSettings.MetadataFallbackLanguage);
        }
        #endregion

        #region TheTVDB Search Code
        /// <summary>
        /// Creates a request object for TheTVDB.com
        /// </summary>
        /// <param name="searchString">String which will be searched on TheTVDB.com</param>
        /// <returns>TheTVDBRequestObject which will be send to TheTVDB.com</returns>
        private TheTVDBRequest CreateTheTVDBRequestObject(string searchString)
        {
            List<RequestObject> requestArray = new List<RequestObject>();
            RequestObject requestObject = new RequestObject()
            {
                IndexName = "TVDB",
                Params = $@"query={searchString}&hitsPerPage=1000"
            };
            requestArray.Add(requestObject);
            TheTVDBRequest theTVDBRequestObject = new TheTVDBRequest()
            {
                Requests = requestArray
            };

            return theTVDBRequestObject;
        }

        /// <summary>
        /// Send a json search request to TheTVDB.com and fetch the data
        /// </summary>
        /// <param name="jsonRequestString">The json request string</param>
        /// <returns>List<TheTVDBResponse> with simplified data from the response for each found entry</returns>
        private async Task<List<TheTVDBResponse>> GetSearchResponseFromTheTVDB(string jsonRequestString)
        {
            string requestLink = @"https://tvshowtime-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.32.0%3Binstantsearch.js%20(3.5.3)%3BJS%20Helper%20(2.28.0)&x-algolia-application-id=tvshowtime&x-algolia-api-key=c9d5ec1316cec12f093754c69dd879d3";
            List<TheTVDBResponse> responseList = new List<TheTVDBResponse>();

            try
            {
                StringContent httpPostContent = new StringContent(jsonRequestString, Encoding.UTF8, "application/x-www-form-urlencoded");
                HttpResponseMessage httpResponse = await httpClient.PostAsync(requestLink, httpPostContent);
                if (httpResponse.Content != null)
                {
                    // Do some magic here :O
                    string jsonResponseString = await httpResponse.Content.ReadAsStringAsync();
                    JObject responseJObject = JObject.Parse(jsonResponseString);
                    List<JToken> responseJToken = responseJObject["results"][0]["hits"].Children().ToList();

                    // Add every entry for tv shows with simplified data in the response list
                    foreach (JToken entry in responseJToken)
                        if (entry.ToObject<TheTVDBResponse>().Type == "series")
                            responseList.Add(entry.ToObject<TheTVDBResponse>());
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }

            return responseList;
        }

        /// <summary>
        /// Send the search request and add the fetched data to list
        /// </summary>
        private async Task SendJsonSearchRequest()
        {
            try
            {
                string searchString = WebUtility.UrlEncode(TbSeriesName.Text); // Replace unsafe characters in URL / Convert URL in a valid ASCII format 
                if (string.IsNullOrWhiteSpace(searchString))
                    return;
                TheTVDBRequest theTVDBRequestObject = CreateTheTVDBRequestObject(searchString);
                string jsonRequestString = JsonConvert.SerializeObject(theTVDBRequestObject);
                fetchedDataList = await GetSearchResponseFromTheTVDB(jsonRequestString);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }
        }

        /// <summary>
        /// Add the retrieved search data (series titles in all available languages) to the listbox
        /// </summary>
        private void AddRetrievedJsonSearchDataToListbox(List<TheTVDBResponse> theTVDBResponseList, string firstLanguage, string fallbackLanguage)
        {
            LbSeriesOnTheTVDB.Items.Clear();
            if (theTVDBResponseList.Count > 0)
            {
                foreach (TheTVDBResponse entry in theTVDBResponseList)
                {
                    if (entry.Translations != null)
                    {
                        JObject jsonObject = JObject.Parse(entry.Translations.ToString());
                        List<JProperty> jTokens = jsonObject.Children().OfType<JProperty>().ToList();

                        JProperty firstLanguageTitle = jTokens.Find(seriesTitle => seriesTitle.Name == firstLanguage);
                        JProperty fallbackLanguageTitle = jTokens.Find(seriesTitle => seriesTitle.Name == fallbackLanguage);

                        if (firstLanguageTitle != null && fallbackLanguageTitle == null)
                            LbSeriesOnTheTVDB.Items.Add($"{firstLanguageTitle.Value} ({firstLanguageTitle.Name})");
                        else if (fallbackLanguageTitle != null && firstLanguageTitle == null)
                            LbSeriesOnTheTVDB.Items.Add($"{fallbackLanguageTitle.Value} ({fallbackLanguageTitle.Name})");
                        else if (firstLanguageTitle != null && fallbackLanguageTitle != null && (string)firstLanguageTitle.Value == (string)fallbackLanguageTitle.Value)
                            LbSeriesOnTheTVDB.Items.Add($"{firstLanguageTitle.Value} ({firstLanguageTitle.Name}/{fallbackLanguageTitle.Name})");
                        else if (firstLanguageTitle != null && fallbackLanguageTitle != null)
                            LbSeriesOnTheTVDB.Items.Add($"{firstLanguageTitle.Value} ({firstLanguageTitle.Name}) / {fallbackLanguageTitle.Value} ({fallbackLanguageTitle.Name})");
                        else
                            LbSeriesOnTheTVDB.Items.Add($"{entry.Name}");
                    }
                }
                BtnGetMetadata.Enabled = true;
                CobOrder.Enabled = true;
            }
        }

        /// <summary>
        /// Get season urls for all orders of a series
        /// </summary>
        /// <param name="seriesURL">The series url</param>
        /// Info: Series URL must be in the format like: "/series/..."
        private void GetSeasonURLsForAllOrdersAndAddToList(string seriesURL)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(seriesURL))
                {
                    HtmlAgilityPack.HtmlDocument seriesDocument = webClientHtmlAgilityPack.Load(seriesURL);
                    HtmlNodeCollection allSeasonHrefs = seriesDocument.DocumentNode.SelectNodes("//div[@class='tab-content']//a[@href]");
                    if (allSeasonHrefs != null)
                    {
                        const string seasonOfficial = @"/seasons/official/";
                        const string seasonDVD = @"/seasons/dvd/";
                        const string seasonAbsolute = @"/seasons/absolute/";
                        const string seasonAlternate = @"/seasons/alternate/";
                        const string seasonUnassigned = @"/unassigned/";

                        foreach (HtmlNode node in allSeasonHrefs)
                        {
                            string hrefValue = node.GetAttributeValue("href", "");
                            if (hrefValue.Contains(seasonOfficial) && !hrefValue.Contains(seasonUnassigned))
                                GetSeasonNumberAndURLAndAddToOrderList(hrefValue, allOrdersSeasonURLs.Official);
                            if (hrefValue.Contains(seasonDVD) && !hrefValue.Contains(seasonUnassigned))
                                GetSeasonNumberAndURLAndAddToOrderList(hrefValue, allOrdersSeasonURLs.DVD);
                            if (hrefValue.Contains(seasonAbsolute) && !hrefValue.Contains(seasonUnassigned))
                                GetSeasonNumberAndURLAndAddToOrderList(hrefValue, allOrdersSeasonURLs.Absolute);
                            if (hrefValue.Contains(seasonAlternate) && !hrefValue.Contains(seasonUnassigned))
                                GetSeasonNumberAndURLAndAddToOrderList(hrefValue, allOrdersSeasonURLs.Alternate);
                        }

                        AddOrderTypesToCombobox();
                        lastSeletedIndexInListbox = LbSeriesOnTheTVDB.SelectedIndex;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }
        }

        /// <summary>
        /// Get season number from season url and add the number and url to the matched order list
        /// </summary>
        /// <param name="seasonURL">The season url from which the season number is splitted</param>
        /// <param name="orderList">Order list to add the season number and url</param>
        private void GetSeasonNumberAndURLAndAddToOrderList(string seasonURL, List<OrderValues> orderList)
        {
            string[] splittedString = seasonURL.Split('/');
            string seasonNumber = splittedString[splittedString.Length - 1];
            orderList.Add(new OrderValues()
            {
                SeasonNumber = int.Parse(seasonNumber),
                SeasonURL = seasonURL
            });
        }

        /// <summary>
        /// Adds every order type available for the specific series to the order combobox
        /// </summary>
        private void AddOrderTypesToCombobox()
        {
            if (allOrdersSeasonURLs.Official.Count > 0)
                CobOrder.Items.Add("Official (Aired) Order");
            if (allOrdersSeasonURLs.DVD.Count > 0)
                CobOrder.Items.Add("DVD Order");
            if (allOrdersSeasonURLs.Absolute.Count > 0)
                CobOrder.Items.Add("Absolute Order");
            if (allOrdersSeasonURLs.Alternate.Count > 0)
                CobOrder.Items.Add("Alternate (Director's Cut) Order");
            if (CobOrder.Items.Count > 0)
                CobOrder.SelectedIndex = 0;
        }

        /// <summary>
        /// Fetch all episoden numbers and episoden urls and add to list
        /// </summary>
        private void GetSeasonInfoForOrderAndAddToList(List<OrderValues> seriesOrderList)
        {
            try
            {
                Regex regex = new Regex(@"[Ss](?<season>\d{1,3})[Ee](?<episode>\d{1,4})");

                foreach (OrderValues season in seriesOrderList)
                {
                    HtmlAgilityPack.HtmlDocument seasonDocument = webClientHtmlAgilityPack.Load(season.SeasonURL);
                    HtmlNodeCollection allEpisodeRows = seasonDocument.DocumentNode.SelectNodes("//tbody//tr/td");
                    HtmlNodeCollection allEpisodeHrefs = seasonDocument.DocumentNode.SelectNodes("//tbody//a[@href]");

                    if (allEpisodeRows != null && allEpisodeRows.Count > 0)
                    {
                        int matchCounter = 0;
                        for (int i = 0; i < allEpisodeRows.Count; i++)
                        {
                            EpisodeRowValues episode = new EpisodeRowValues();
                            Match match = regex.Match(allEpisodeRows[i].InnerText);
                            if (match.Success)
                            {
                                episode.SeasonNumber = int.Parse(match.Groups["season"].Value);
                                episode.EpisodeNumber = int.Parse(match.Groups["episode"].Value);
                                episode.EpisodeURL = theTVDBWebsiteURL + allEpisodeHrefs[matchCounter].GetAttributeValue("href", "");
                                matchCounter++;
                                episodeRowValuesForAllSeasonsOnOrder.Add(episode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }
        }

        /// <summary>
        /// Fetch every episode titles for one season and add to list
        /// </summary>
        /// <param name="seasonEpisodeList">Season list with episode urls</param>
        private void GetEpisodeInfoAndAddToList(List<EpisodeRowValues> seasonEpisodeList)
        {
            LbEpisodeNames.Items.Clear();
            if (seasonEpisodeList.Count > 0)
            {
                try
                {
                    foreach (EpisodeRowValues episode in seasonEpisodeList)
                    {
                        HtmlAgilityPack.HtmlDocument episodeDocument = webClientHtmlAgilityPack.Load(episode.EpisodeURL);
                        HtmlNodeCollection allTranslationRows = episodeDocument.DocumentNode.SelectNodes("//div[@class='change_translation_text']");
                        List<TranslatedEpisode> translatedEpisodeTitlesList = new List<TranslatedEpisode>();
                        int seasonNumber = episode.SeasonNumber;
                        int episodeNumber = episode.EpisodeNumber;
                        if (allTranslationRows != null && allTranslationRows.Count > 0)
                        {
                            foreach (HtmlNode languageRow in allTranslationRows)
                            {
                                string languageType = WebUtility.HtmlDecode(languageRow.GetAttributeValue("data-language", ""));
                                TranslatedEpisode translatedEpisodeTitle = new TranslatedEpisode()
                                {
                                    SeasonNumber = seasonNumber,
                                    EpisodeNumber = episodeNumber,
                                    LanguageType = languageType,
                                    TranslatedEpisodeTitle = WebUtility.HtmlDecode(languageRow.GetAttributeValue("data-title", ""))
                                };
                                translatedEpisodeTitlesList.Add(translatedEpisodeTitle);
                            }
                            translatedEpisodeTitlesForAllSeasons.Add(translatedEpisodeTitlesList);
                        }
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }
            }
        }

        /// <summary>
        /// Add translated episode titles to list
        /// </summary>
        /// <param name="translatedEpisodeTitlesList"></param>
        /// <param name="firstLanguageType">First language which will be used for titles</param>
        /// <param name="fallbackLanguageType">Language which will be used if first language title is not available</param>
        private void AddEpisodeNamesToListbox(List<List<TranslatedEpisode>> translatedEpisodeTitlesList, string firstLanguageType, string fallbackLanguageType)
        {
            LblLoadMetadataInfo.Text = "";
            translatedEpisodeTitlesForOneSeason.Clear();

            List<TranslatedEpisode> firstLanguageTitles = new List<TranslatedEpisode>();
            List<TranslatedEpisode> fallbackLanguageTitles = new List<TranslatedEpisode>();
            int episodeCount = 0;

            foreach (List<TranslatedEpisode> translatedEpisodes in translatedEpisodeTitlesList)
            {
                foreach (TranslatedEpisode titlesInAllAvailableLanguages in translatedEpisodes)
                {
                    if (titlesInAllAvailableLanguages.SeasonNumber == (int)NumSeasonNumber.Value && titlesInAllAvailableLanguages.LanguageType == firstLanguageType)
                        firstLanguageTitles.Add(titlesInAllAvailableLanguages);
                    if (titlesInAllAvailableLanguages.SeasonNumber == (int)NumSeasonNumber.Value && titlesInAllAvailableLanguages.LanguageType == fallbackLanguageType)
                        fallbackLanguageTitles.Add(titlesInAllAvailableLanguages);
                    // Get episode count
                    if (titlesInAllAvailableLanguages.SeasonNumber == (int)NumSeasonNumber.Value)
                        episodeCount = titlesInAllAvailableLanguages.EpisodeNumber;
                }
            }

            for (int i = 0; i < episodeCount; i++)
            {
                TranslatedEpisode firstLang = firstLanguageTitles.Find(x => x.EpisodeNumber == i + 1);
                TranslatedEpisode fallbackLang = fallbackLanguageTitles.Find(x => x.EpisodeNumber == i + 1);

                if (firstLang != null && !string.IsNullOrWhiteSpace(firstLang.TranslatedEpisodeTitle))
                {
                    string episodeTitle = RemoveInvalidChars(firstLang.TranslatedEpisodeTitle);
                    LbEpisodeNames.Items.Add($"S{firstLang.SeasonNumber}E{firstLang.EpisodeNumber} - {firstLang.LanguageType}: {episodeTitle}");
                    translatedEpisodeTitlesForOneSeason.Add(new TranslatedEpisode()
                    {
                        SeasonNumber = firstLang.SeasonNumber,
                        EpisodeNumber = firstLang.EpisodeNumber,
                        TranslatedEpisodeTitle = episodeTitle
                    });
                }
                else if (fallbackLang != null && !string.IsNullOrWhiteSpace(fallbackLang.TranslatedEpisodeTitle))
                {
                    string episodeTitle = RemoveInvalidChars(fallbackLang.TranslatedEpisodeTitle);
                    LbEpisodeNames.Items.Add($"S{fallbackLang.SeasonNumber}E{fallbackLang.EpisodeNumber} - {fallbackLang.LanguageType}: {episodeTitle}");
                    translatedEpisodeTitlesForOneSeason.Add(new TranslatedEpisode()
                    {
                        SeasonNumber = fallbackLang.SeasonNumber,
                        EpisodeNumber = fallbackLang.EpisodeNumber,
                        TranslatedEpisodeTitle = episodeTitle
                    });
                }
                else
                {
                    string episodeTitle = Globals.noEpisodeInfoOrTitleAvailableText;
                    int seasonNumber = (int)NumSeasonNumber.Value;
                    int episodeNumber = i + 1;
                    LbEpisodeNames.Items.Add($"S{seasonNumber}E{episodeNumber} - {episodeTitle}");
                    translatedEpisodeTitlesForOneSeason.Add(new TranslatedEpisode()
                    {
                        SeasonNumber = seasonNumber,
                        EpisodeNumber = episodeNumber,
                        TranslatedEpisodeTitle = episodeTitle
                    });
                }
            }

            LblEpisodeCount.Text = "Folgenanzahl:";
            LblEpisodeCountCounter.Text = LbEpisodeNames.Items.Count.ToString();
        }
        #endregion

        // Enable Double-Buffer for all controls of the form
        // "Fixes" Combobox flickering
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
    }

    public class StrCmpLogicalComparer : Comparer<string>
    {
        [DllImport("Shlwapi.dll", CharSet = CharSet.Unicode)]
        private static extern int StrCmpLogicalW(string x, string y);

        public override int Compare(string x, string y)
        {
            return StrCmpLogicalW(x, y);
        }
    }
}
