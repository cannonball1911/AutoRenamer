﻿namespace AutoRenamer
{
    partial class CreateFolderWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateFolderWindow));
            this.GrbOne = new System.Windows.Forms.GroupBox();
            this.LbFolderPath = new System.Windows.Forms.ListBox();
            this.LblFolderPathInfo = new System.Windows.Forms.Label();
            this.BtnFolderSelection = new AutoRenamer.Classes.DarkMode.DarkModeButton();
            this.MstMain = new System.Windows.Forms.MenuStrip();
            this.MstItemOrdner = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemFolderSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.GrbTwo = new System.Windows.Forms.GroupBox();
            this.NumSeasonNumber = new System.Windows.Forms.NumericUpDown();
            this.CbCreateS00 = new System.Windows.Forms.CheckBox();
            this.LblCreateS00 = new System.Windows.Forms.Label();
            this.LblSeasonNumber = new System.Windows.Forms.Label();
            this.GrbThree = new System.Windows.Forms.GroupBox();
            this.BtnCreateFolders = new AutoRenamer.Classes.DarkMode.DarkModeButton();
            this.GrbOne.SuspendLayout();
            this.MstMain.SuspendLayout();
            this.GrbTwo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumSeasonNumber)).BeginInit();
            this.GrbThree.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrbOne
            // 
            this.GrbOne.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrbOne.BackColor = System.Drawing.SystemColors.Control;
            this.GrbOne.Controls.Add(this.LbFolderPath);
            this.GrbOne.Controls.Add(this.LblFolderPathInfo);
            this.GrbOne.Controls.Add(this.BtnFolderSelection);
            this.GrbOne.Location = new System.Drawing.Point(12, 27);
            this.GrbOne.Name = "GrbOne";
            this.GrbOne.Size = new System.Drawing.Size(560, 135);
            this.GrbOne.TabIndex = 3;
            this.GrbOne.TabStop = false;
            this.GrbOne.Text = "1. Ordnerauswahl";
            // 
            // LbFolderPath
            // 
            this.LbFolderPath.AllowDrop = true;
            this.LbFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LbFolderPath.FormattingEnabled = true;
            this.LbFolderPath.HorizontalScrollbar = true;
            this.LbFolderPath.Location = new System.Drawing.Point(4, 83);
            this.LbFolderPath.Margin = new System.Windows.Forms.Padding(2);
            this.LbFolderPath.Name = "LbFolderPath";
            this.LbFolderPath.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.LbFolderPath.Size = new System.Drawing.Size(553, 43);
            this.LbFolderPath.TabIndex = 4;
            this.LbFolderPath.TabStop = false;
            this.LbFolderPath.UseTabStops = false;
            this.LbFolderPath.DragDrop += new System.Windows.Forms.DragEventHandler(this.LbFolderPath_DragDrop);
            this.LbFolderPath.DragEnter += new System.Windows.Forms.DragEventHandler(this.LbFolderPath_DragEnter);
            // 
            // LblFolderPathInfo
            // 
            this.LblFolderPathInfo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblFolderPathInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.LblFolderPathInfo.Location = new System.Drawing.Point(202, 64);
            this.LblFolderPathInfo.Name = "LblFolderPathInfo";
            this.LblFolderPathInfo.Size = new System.Drawing.Size(161, 16);
            this.LblFolderPathInfo.TabIndex = 3;
            this.LblFolderPathInfo.Text = "Ordner werden erstellt in:";
            // 
            // BtnFolderSelection
            // 
            this.BtnFolderSelection.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnFolderSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFolderSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFolderSelection.Location = new System.Drawing.Point(177, 19);
            this.BtnFolderSelection.Name = "BtnFolderSelection";
            this.BtnFolderSelection.Size = new System.Drawing.Size(203, 34);
            this.BtnFolderSelection.TabIndex = 1;
            this.BtnFolderSelection.Text = "Ordnerauswahl";
            this.BtnFolderSelection.UseVisualStyleBackColor = true;
            this.BtnFolderSelection.Click += new System.EventHandler(this.BtnFolderSelection_Click);
            // 
            // MstMain
            // 
            this.MstMain.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.MstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemOrdner});
            this.MstMain.Location = new System.Drawing.Point(0, 0);
            this.MstMain.Name = "MstMain";
            this.MstMain.Size = new System.Drawing.Size(584, 24);
            this.MstMain.TabIndex = 6;
            this.MstMain.Text = "menuStrip1";
            // 
            // MstItemOrdner
            // 
            this.MstItemOrdner.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemFolderSelection});
            this.MstItemOrdner.Name = "MstItemOrdner";
            this.MstItemOrdner.Size = new System.Drawing.Size(56, 20);
            this.MstItemOrdner.Text = "&Ordner";
            // 
            // MstItemFolderSelection
            // 
            this.MstItemFolderSelection.Name = "MstItemFolderSelection";
            this.MstItemFolderSelection.Size = new System.Drawing.Size(119, 22);
            this.MstItemFolderSelection.Text = "&Auswahl";
            this.MstItemFolderSelection.Click += new System.EventHandler(this.MstItemFolderSelection_Click);
            // 
            // GrbTwo
            // 
            this.GrbTwo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrbTwo.Controls.Add(this.NumSeasonNumber);
            this.GrbTwo.Controls.Add(this.CbCreateS00);
            this.GrbTwo.Controls.Add(this.LblCreateS00);
            this.GrbTwo.Controls.Add(this.LblSeasonNumber);
            this.GrbTwo.Location = new System.Drawing.Point(12, 176);
            this.GrbTwo.Name = "GrbTwo";
            this.GrbTwo.Size = new System.Drawing.Size(560, 129);
            this.GrbTwo.TabIndex = 7;
            this.GrbTwo.TabStop = false;
            this.GrbTwo.Text = "2. Staffelanzahl / Ordneranzahl";
            // 
            // NumSeasonNumber
            // 
            this.NumSeasonNumber.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.NumSeasonNumber.Location = new System.Drawing.Point(241, 90);
            this.NumSeasonNumber.Margin = new System.Windows.Forms.Padding(2);
            this.NumSeasonNumber.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.NumSeasonNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumSeasonNumber.Name = "NumSeasonNumber";
            this.NumSeasonNumber.Size = new System.Drawing.Size(89, 20);
            this.NumSeasonNumber.TabIndex = 3;
            this.NumSeasonNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumSeasonNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // CbCreateS00
            // 
            this.CbCreateS00.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.CbCreateS00.AutoSize = true;
            this.CbCreateS00.Location = new System.Drawing.Point(273, 41);
            this.CbCreateS00.Name = "CbCreateS00";
            this.CbCreateS00.Size = new System.Drawing.Size(15, 14);
            this.CbCreateS00.TabIndex = 2;
            this.CbCreateS00.UseVisualStyleBackColor = true;
            // 
            // LblCreateS00
            // 
            this.LblCreateS00.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.LblCreateS00.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCreateS00.Location = new System.Drawing.Point(173, 16);
            this.LblCreateS00.Name = "LblCreateS00";
            this.LblCreateS00.Size = new System.Drawing.Size(213, 22);
            this.LblCreateS00.TabIndex = 8;
            this.LblCreateS00.Text = "Erstelle Staffel 00? (Season 00)";
            this.LblCreateS00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblSeasonNumber
            // 
            this.LblSeasonNumber.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.LblSeasonNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSeasonNumber.Location = new System.Drawing.Point(189, 67);
            this.LblSeasonNumber.Name = "LblSeasonNumber";
            this.LblSeasonNumber.Size = new System.Drawing.Size(185, 22);
            this.LblSeasonNumber.TabIndex = 7;
            this.LblSeasonNumber.Text = "Staffelanzahl (ab Season 01)";
            this.LblSeasonNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GrbThree
            // 
            this.GrbThree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrbThree.Controls.Add(this.BtnCreateFolders);
            this.GrbThree.Location = new System.Drawing.Point(12, 311);
            this.GrbThree.Name = "GrbThree";
            this.GrbThree.Size = new System.Drawing.Size(560, 65);
            this.GrbThree.TabIndex = 8;
            this.GrbThree.TabStop = false;
            this.GrbThree.Text = "3. Erstellen";
            // 
            // BtnCreateFolders
            // 
            this.BtnCreateFolders.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BtnCreateFolders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCreateFolders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCreateFolders.Location = new System.Drawing.Point(177, 19);
            this.BtnCreateFolders.Name = "BtnCreateFolders";
            this.BtnCreateFolders.Size = new System.Drawing.Size(203, 34);
            this.BtnCreateFolders.TabIndex = 4;
            this.BtnCreateFolders.Text = "Ordner erstellen";
            this.BtnCreateFolders.UseVisualStyleBackColor = true;
            this.BtnCreateFolders.Click += new System.EventHandler(this.BtnCreateFolders_Click);
            // 
            // CreateFolderWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 386);
            this.Controls.Add(this.GrbThree);
            this.Controls.Add(this.GrbTwo);
            this.Controls.Add(this.MstMain);
            this.Controls.Add(this.GrbOne);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 425);
            this.Name = "CreateFolderWindow";
            this.Text = "AutoRenamer: Create Folder";
            this.GrbOne.ResumeLayout(false);
            this.MstMain.ResumeLayout(false);
            this.MstMain.PerformLayout();
            this.GrbTwo.ResumeLayout(false);
            this.GrbTwo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumSeasonNumber)).EndInit();
            this.GrbThree.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GrbOne;
        private Classes.DarkMode.DarkModeButton BtnFolderSelection;
        private System.Windows.Forms.MenuStrip MstMain;
        private System.Windows.Forms.ToolStripMenuItem MstItemOrdner;
        private System.Windows.Forms.ToolStripMenuItem MstItemFolderSelection;
        private System.Windows.Forms.Label LblFolderPathInfo;
        private System.Windows.Forms.GroupBox GrbTwo;
        private System.Windows.Forms.Label LblSeasonNumber;
        private System.Windows.Forms.Label LblCreateS00;
        private System.Windows.Forms.CheckBox CbCreateS00;
        private System.Windows.Forms.GroupBox GrbThree;
        private Classes.DarkMode.DarkModeButton BtnCreateFolders;
        private System.Windows.Forms.ListBox LbFolderPath;
        private System.Windows.Forms.NumericUpDown NumSeasonNumber;
    }
}