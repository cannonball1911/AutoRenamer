﻿namespace AutoRenamer
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsWindow));
            this.TbcSettings = new System.Windows.Forms.TabControl();
            this.TbpMetadata = new System.Windows.Forms.TabPage();
            this.CbxAddEpisodenameToFilename = new System.Windows.Forms.CheckBox();
            this.LblAddEpisodenameToFileDetailsInfo = new System.Windows.Forms.Label();
            this.CbxAddEpisodenameToFileDetails = new System.Windows.Forms.CheckBox();
            this.GrbMetadataLanguage = new System.Windows.Forms.GroupBox();
            this.CobMetadataFirstLanguage = new System.Windows.Forms.ComboBox();
            this.CobMetadataFallbackLanguage = new System.Windows.Forms.ComboBox();
            this.LblMetadataFirstLanguage = new System.Windows.Forms.Label();
            this.LblMetadataFallbackLanguage = new System.Windows.Forms.Label();
            this.TbpUI = new System.Windows.Forms.TabPage();
            this.CbxUIDarkMode = new System.Windows.Forms.CheckBox();
            this.BtnSettingsSaveSettings = new AutoRenamer.Classes.DarkMode.DarkModeButton();
            this.TbcSettings.SuspendLayout();
            this.TbpMetadata.SuspendLayout();
            this.GrbMetadataLanguage.SuspendLayout();
            this.TbpUI.SuspendLayout();
            this.SuspendLayout();
            // 
            // TbcSettings
            // 
            this.TbcSettings.Controls.Add(this.TbpMetadata);
            this.TbcSettings.Controls.Add(this.TbpUI);
            this.TbcSettings.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.TbcSettings.Location = new System.Drawing.Point(12, 12);
            this.TbcSettings.Name = "TbcSettings";
            this.TbcSettings.SelectedIndex = 0;
            this.TbcSettings.Size = new System.Drawing.Size(660, 459);
            this.TbcSettings.TabIndex = 0;
            this.TbcSettings.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.TbcSettings_DrawItem);
            // 
            // TbpMetadata
            // 
            this.TbpMetadata.Controls.Add(this.CbxAddEpisodenameToFilename);
            this.TbpMetadata.Controls.Add(this.LblAddEpisodenameToFileDetailsInfo);
            this.TbpMetadata.Controls.Add(this.CbxAddEpisodenameToFileDetails);
            this.TbpMetadata.Controls.Add(this.GrbMetadataLanguage);
            this.TbpMetadata.Location = new System.Drawing.Point(4, 22);
            this.TbpMetadata.Name = "TbpMetadata";
            this.TbpMetadata.Padding = new System.Windows.Forms.Padding(3);
            this.TbpMetadata.Size = new System.Drawing.Size(652, 433);
            this.TbpMetadata.TabIndex = 0;
            this.TbpMetadata.Text = "Metadata";
            this.TbpMetadata.UseVisualStyleBackColor = true;
            // 
            // CbxAddEpisodenameToFilename
            // 
            this.CbxAddEpisodenameToFilename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CbxAddEpisodenameToFilename.AutoSize = true;
            this.CbxAddEpisodenameToFilename.Location = new System.Drawing.Point(6, 6);
            this.CbxAddEpisodenameToFilename.Name = "CbxAddEpisodenameToFilename";
            this.CbxAddEpisodenameToFilename.Size = new System.Drawing.Size(215, 17);
            this.CbxAddEpisodenameToFilename.TabIndex = 10;
            this.CbxAddEpisodenameToFilename.Text = "Hänge Episodenname an Dateiname an";
            this.CbxAddEpisodenameToFilename.UseVisualStyleBackColor = true;
            this.CbxAddEpisodenameToFilename.CheckedChanged += new System.EventHandler(this.CbxAddEpisodenameToFilename_CheckedChanged);
            // 
            // LblAddEpisodenameToFileDetailsInfo
            // 
            this.LblAddEpisodenameToFileDetailsInfo.Location = new System.Drawing.Point(3, 165);
            this.LblAddEpisodenameToFileDetailsInfo.Name = "LblAddEpisodenameToFileDetailsInfo";
            this.LblAddEpisodenameToFileDetailsInfo.Size = new System.Drawing.Size(650, 49);
            this.LblAddEpisodenameToFileDetailsInfo.TabIndex = 6;
            this.LblAddEpisodenameToFileDetailsInfo.Text = resources.GetString("LblAddEpisodenameToFileDetailsInfo.Text");
            // 
            // CbxAddEpisodenameToFileDetails
            // 
            this.CbxAddEpisodenameToFileDetails.Location = new System.Drawing.Point(6, 132);
            this.CbxAddEpisodenameToFileDetails.Name = "CbxAddEpisodenameToFileDetails";
            this.CbxAddEpisodenameToFileDetails.Size = new System.Drawing.Size(647, 30);
            this.CbxAddEpisodenameToFileDetails.TabIndex = 5;
            this.CbxAddEpisodenameToFileDetails.Text = "Füge Episodenname zu den Dateieingenschaften-Titel hinzu (Experimental / Use at y" +
    "our own risk!)";
            this.CbxAddEpisodenameToFileDetails.UseVisualStyleBackColor = true;
            this.CbxAddEpisodenameToFileDetails.CheckedChanged += new System.EventHandler(this.CbxAddEpisodenameToFileDetails_CheckedChanged);
            // 
            // GrbMetadataLanguage
            // 
            this.GrbMetadataLanguage.Controls.Add(this.CobMetadataFirstLanguage);
            this.GrbMetadataLanguage.Controls.Add(this.CobMetadataFallbackLanguage);
            this.GrbMetadataLanguage.Controls.Add(this.LblMetadataFirstLanguage);
            this.GrbMetadataLanguage.Controls.Add(this.LblMetadataFallbackLanguage);
            this.GrbMetadataLanguage.Location = new System.Drawing.Point(6, 29);
            this.GrbMetadataLanguage.Name = "GrbMetadataLanguage";
            this.GrbMetadataLanguage.Size = new System.Drawing.Size(276, 84);
            this.GrbMetadataLanguage.TabIndex = 4;
            this.GrbMetadataLanguage.TabStop = false;
            this.GrbMetadataLanguage.Text = "Sprachauswahl";
            // 
            // CobMetadataFirstLanguage
            // 
            this.CobMetadataFirstLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobMetadataFirstLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CobMetadataFirstLanguage.FormattingEnabled = true;
            this.CobMetadataFirstLanguage.Location = new System.Drawing.Point(106, 19);
            this.CobMetadataFirstLanguage.Name = "CobMetadataFirstLanguage";
            this.CobMetadataFirstLanguage.Size = new System.Drawing.Size(159, 21);
            this.CobMetadataFirstLanguage.TabIndex = 1;
            this.CobMetadataFirstLanguage.SelectedIndexChanged += new System.EventHandler(this.CobMetadataFirstLanguage_SelectedIndexChanged);
            // 
            // CobMetadataFallbackLanguage
            // 
            this.CobMetadataFallbackLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobMetadataFallbackLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CobMetadataFallbackLanguage.FormattingEnabled = true;
            this.CobMetadataFallbackLanguage.Location = new System.Drawing.Point(106, 46);
            this.CobMetadataFallbackLanguage.Name = "CobMetadataFallbackLanguage";
            this.CobMetadataFallbackLanguage.Size = new System.Drawing.Size(159, 21);
            this.CobMetadataFallbackLanguage.TabIndex = 3;
            this.CobMetadataFallbackLanguage.SelectedIndexChanged += new System.EventHandler(this.CobMetadataFallbackLanguage_SelectedIndexChanged);
            // 
            // LblMetadataFirstLanguage
            // 
            this.LblMetadataFirstLanguage.AutoSize = true;
            this.LblMetadataFirstLanguage.Location = new System.Drawing.Point(7, 22);
            this.LblMetadataFirstLanguage.Name = "LblMetadataFirstLanguage";
            this.LblMetadataFirstLanguage.Size = new System.Drawing.Size(77, 13);
            this.LblMetadataFirstLanguage.TabIndex = 0;
            this.LblMetadataFirstLanguage.Text = "Hauptsprache:";
            // 
            // LblMetadataFallbackLanguage
            // 
            this.LblMetadataFallbackLanguage.AutoSize = true;
            this.LblMetadataFallbackLanguage.Location = new System.Drawing.Point(7, 49);
            this.LblMetadataFallbackLanguage.Name = "LblMetadataFallbackLanguage";
            this.LblMetadataFallbackLanguage.Size = new System.Drawing.Size(93, 13);
            this.LblMetadataFallbackLanguage.TabIndex = 2;
            this.LblMetadataFallbackLanguage.Text = "Fallback Sprache:";
            // 
            // TbpUI
            // 
            this.TbpUI.Controls.Add(this.CbxUIDarkMode);
            this.TbpUI.Location = new System.Drawing.Point(4, 22);
            this.TbpUI.Name = "TbpUI";
            this.TbpUI.Size = new System.Drawing.Size(652, 433);
            this.TbpUI.TabIndex = 1;
            this.TbpUI.Text = "UI";
            this.TbpUI.UseVisualStyleBackColor = true;
            // 
            // CbxUIDarkMode
            // 
            this.CbxUIDarkMode.AutoSize = true;
            this.CbxUIDarkMode.Location = new System.Drawing.Point(13, 13);
            this.CbxUIDarkMode.Name = "CbxUIDarkMode";
            this.CbxUIDarkMode.Size = new System.Drawing.Size(98, 17);
            this.CbxUIDarkMode.TabIndex = 0;
            this.CbxUIDarkMode.Text = "Dunkler Modus";
            this.CbxUIDarkMode.UseVisualStyleBackColor = true;
            this.CbxUIDarkMode.CheckedChanged += new System.EventHandler(this.CbxUIDarkMode_CheckedChanged);
            // 
            // BtnSettingsSaveSettings
            // 
            this.BtnSettingsSaveSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSettingsSaveSettings.Location = new System.Drawing.Point(522, 477);
            this.BtnSettingsSaveSettings.Name = "BtnSettingsSaveSettings";
            this.BtnSettingsSaveSettings.Size = new System.Drawing.Size(150, 23);
            this.BtnSettingsSaveSettings.TabIndex = 1;
            this.BtnSettingsSaveSettings.Text = "Einstellungen speichern";
            this.BtnSettingsSaveSettings.UseVisualStyleBackColor = true;
            this.BtnSettingsSaveSettings.Click += new System.EventHandler(this.BtnSettingsSaveSettings_Click);
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 508);
            this.Controls.Add(this.BtnSettingsSaveSettings);
            this.Controls.Add(this.TbcSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsWindow";
            this.Text = "AutoRenamer: Settings";
            this.TbcSettings.ResumeLayout(false);
            this.TbpMetadata.ResumeLayout(false);
            this.TbpMetadata.PerformLayout();
            this.GrbMetadataLanguage.ResumeLayout(false);
            this.GrbMetadataLanguage.PerformLayout();
            this.TbpUI.ResumeLayout(false);
            this.TbpUI.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TbcSettings;
        private System.Windows.Forms.TabPage TbpMetadata;
        private System.Windows.Forms.GroupBox GrbMetadataLanguage;
        private System.Windows.Forms.ComboBox CobMetadataFirstLanguage;
        private System.Windows.Forms.ComboBox CobMetadataFallbackLanguage;
        private System.Windows.Forms.Label LblMetadataFirstLanguage;
        private System.Windows.Forms.Label LblMetadataFallbackLanguage;
        private Classes.DarkMode.DarkModeButton BtnSettingsSaveSettings;
        private System.Windows.Forms.Label LblAddEpisodenameToFileDetailsInfo;
        private System.Windows.Forms.CheckBox CbxAddEpisodenameToFileDetails;
        private System.Windows.Forms.CheckBox CbxAddEpisodenameToFilename;
        private System.Windows.Forms.TabPage TbpUI;
        private System.Windows.Forms.CheckBox CbxUIDarkMode;
    }
}