﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

/* Solution: AutoRenamer, CreateFolder.cs
 * Creates the season folders for the number of seasons in a series
 * Kai Sackl, 20.02.2019
 */

namespace AutoRenamer
{
    public partial class CreateFolderWindow : Form
    {
        private string folderPath;

        public CreateFolderWindow()
        {
            InitializeComponent();
            Init();
        }

        #region Button Events
        private void BtnFolderSelection_Click(object sender, EventArgs e)
        {
            Clear();
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {                  
                    LbFolderPath.Items.Add(fbd.SelectedPath);
                    folderPath = fbd.SelectedPath;
                    BtnFolderSelection.BackColor = Color.LightGreen;
                }
            }
        }

        private void BtnCreateFolders_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(folderPath))
            {
                int numberOfFolders = Convert.ToInt32(NumSeasonNumber.Value);
                int season00 = CbCreateS00.Checked ? 0 : 1;

                // If createS00 is checked(true), start with 0, else 1
                for (int i = season00; i <= numberOfFolders; i++)
                {
                    string folderName = i < 10 ? $"Season 0{i}" : $"Season {i}";

                    try
                    {
                        Directory.CreateDirectory(Path.Combine(folderPath, folderName));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Task failed successfully!");
                        return;
                    }
                }
                Clear();
                return;
            }
            MessageBox.Show("Kein Ordner ausgewählt!", "Task failed successfully!");
        }
        #endregion

        #region DragAndDrop Events
        private void LbFolderPath_DragEnter(object sender, DragEventArgs e)
        {
            string[] filesAndFolders = (string[])e.Data.GetData(DataFormats.FileDrop);
            bool isFolder = File.GetAttributes(filesAndFolders[0]).HasFlag(FileAttributes.Directory);

            e.Effect = filesAndFolders.Length == 1 && isFolder ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void LbFolderPath_DragDrop(object sender, DragEventArgs e)
        {
            Clear();
            string[] filesAndFolders = (string[])e.Data.GetData(DataFormats.FileDrop);
            LbFolderPath.Items.Add(filesAndFolders[0]);
            folderPath = filesAndFolders[0];
            BtnFolderSelection.BackColor = Color.LightGreen;
        }
        #endregion

        #region MenuStrip Events
        private void MstItemFolderSelection_Click(object sender, EventArgs e) => BtnFolderSelection.PerformClick();
        #endregion

        #region Methods
        /// <summary>
        /// Clears the folderpath listbox and
        /// sets the button (BtnFolderSelection) to default color
        /// </summary>
        private void Clear()
        {
            LbFolderPath.Items.Clear();
            folderPath = "";
            Globals.SetButtonDefaultColor(BtnFolderSelection);
        }

        private void Init()
        {
            if (Globals.userSettings.DarkMode)
                Globals.ChangeToDarkMode(this, true);
            else
                Globals.ChangeToDarkMode(this, false);
        }
        #endregion
    }
}
