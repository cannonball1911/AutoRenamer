﻿namespace AutoRenamer
{
    partial class InitWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InitWindow));
            this.LbSelection = new System.Windows.Forms.ListBox();
            this.GrbOne = new System.Windows.Forms.GroupBox();
            this.BtnFileSelection = new Classes.DarkMode.DarkModeButton();
            this.LblFileCounter = new System.Windows.Forms.Label();
            this.GrbTwo = new System.Windows.Forms.GroupBox();
            this.LblEpisodeCountCounter = new System.Windows.Forms.Label();
            this.LblEpisodeCount = new System.Windows.Forms.Label();
            this.LblLoadMetadataInfo = new System.Windows.Forms.Label();
            this.BtnGetMetadata = new Classes.DarkMode.DarkModeButton();
            this.BtnClearSearch = new Classes.DarkMode.DarkModeButton();
            this.CobOrder = new System.Windows.Forms.ComboBox();
            this.LblOrder = new System.Windows.Forms.Label();
            this.LblEpisodeNames = new System.Windows.Forms.Label();
            this.LbEpisodeNames = new System.Windows.Forms.ListBox();
            this.PbxThumbnail = new System.Windows.Forms.PictureBox();
            this.BtnSearchOnTheTVDB = new Classes.DarkMode.DarkModeButton();
            this.LblFetchFromTheTVDB = new System.Windows.Forms.Label();
            this.LbSeriesOnTheTVDB = new System.Windows.Forms.ListBox();
            this.NumSeasonNumber = new KaisLib.Controls.NumericUpDownLeadingZero();
            this.LblFirstEpisodeNumber = new System.Windows.Forms.Label();
            this.NumFirstEpisodeNumber = new KaisLib.Controls.NumericUpDownLeadingZero();
            this.TbSeriesName = new KaisLib.Controls.WatermarkedTextBox();
            this.LblSeasonNumber = new System.Windows.Forms.Label();
            this.LblSeriesName = new System.Windows.Forms.Label();
            this.GrbThree = new System.Windows.Forms.GroupBox();
            this.BtnStart = new Classes.DarkMode.DarkModeButton();
            this.MstMain = new System.Windows.Forms.MenuStrip();
            this.MstItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemFileSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemFolderCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemOptionsSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MstItemHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.GrbOne.SuspendLayout();
            this.GrbTwo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxThumbnail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumSeasonNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumFirstEpisodeNumber)).BeginInit();
            this.GrbThree.SuspendLayout();
            this.MstMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // LbSelection
            // 
            this.LbSelection.AllowDrop = true;
            this.LbSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LbSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbSelection.FormattingEnabled = true;
            this.LbSelection.HorizontalScrollbar = true;
            this.LbSelection.ItemHeight = 15;
            this.LbSelection.Location = new System.Drawing.Point(6, 65);
            this.LbSelection.Name = "LbSelection";
            this.LbSelection.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.LbSelection.Size = new System.Drawing.Size(898, 154);
            this.LbSelection.TabIndex = 0;
            this.LbSelection.TabStop = false;
            this.LbSelection.DragDrop += new System.Windows.Forms.DragEventHandler(this.LbAuswahl_DragDrop);
            this.LbSelection.DragEnter += new System.Windows.Forms.DragEventHandler(this.LbAuswahl_DragEnter);
            // 
            // GrbOne
            // 
            this.GrbOne.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrbOne.BackColor = System.Drawing.SystemColors.Control;
            this.GrbOne.Controls.Add(this.BtnFileSelection);
            this.GrbOne.Controls.Add(this.LblFileCounter);
            this.GrbOne.Controls.Add(this.LbSelection);
            this.GrbOne.Location = new System.Drawing.Point(12, 27);
            this.GrbOne.Name = "GrbOne";
            this.GrbOne.Size = new System.Drawing.Size(910, 233);
            this.GrbOne.TabIndex = 2;
            this.GrbOne.TabStop = false;
            this.GrbOne.Text = "1. Dateiauswahl";
            // 
            // BtnFileSelection
            // 
            this.BtnFileSelection.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnFileSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFileSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.BtnFileSelection.Location = new System.Drawing.Point(352, 19);
            this.BtnFileSelection.Name = "BtnFileSelection";
            this.BtnFileSelection.Size = new System.Drawing.Size(203, 34);
            this.BtnFileSelection.TabIndex = 3;
            this.BtnFileSelection.Text = "Dateiauswahl";
            this.BtnFileSelection.UseVisualStyleBackColor = true;
            this.BtnFileSelection.Click += new System.EventHandler(this.BtnFileSelection_Click);
            // 
            // LblFileCounter
            // 
            this.LblFileCounter.Location = new System.Drawing.Point(6, 42);
            this.LblFileCounter.Name = "LblFileCounter";
            this.LblFileCounter.Size = new System.Drawing.Size(165, 16);
            this.LblFileCounter.TabIndex = 2;
            // 
            // GrbTwo
            // 
            this.GrbTwo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrbTwo.Controls.Add(this.LblEpisodeCountCounter);
            this.GrbTwo.Controls.Add(this.LblEpisodeCount);
            this.GrbTwo.Controls.Add(this.LblLoadMetadataInfo);
            this.GrbTwo.Controls.Add(this.BtnGetMetadata);
            this.GrbTwo.Controls.Add(this.BtnClearSearch);
            this.GrbTwo.Controls.Add(this.CobOrder);
            this.GrbTwo.Controls.Add(this.LblOrder);
            this.GrbTwo.Controls.Add(this.LblEpisodeNames);
            this.GrbTwo.Controls.Add(this.LbEpisodeNames);
            this.GrbTwo.Controls.Add(this.PbxThumbnail);
            this.GrbTwo.Controls.Add(this.BtnSearchOnTheTVDB);
            this.GrbTwo.Controls.Add(this.LblFetchFromTheTVDB);
            this.GrbTwo.Controls.Add(this.LbSeriesOnTheTVDB);
            this.GrbTwo.Controls.Add(this.NumSeasonNumber);
            this.GrbTwo.Controls.Add(this.LblFirstEpisodeNumber);
            this.GrbTwo.Controls.Add(this.NumFirstEpisodeNumber);
            this.GrbTwo.Controls.Add(this.TbSeriesName);
            this.GrbTwo.Controls.Add(this.LblSeasonNumber);
            this.GrbTwo.Controls.Add(this.LblSeriesName);
            this.GrbTwo.Location = new System.Drawing.Point(12, 266);
            this.GrbTwo.Name = "GrbTwo";
            this.GrbTwo.Size = new System.Drawing.Size(910, 315);
            this.GrbTwo.TabIndex = 3;
            this.GrbTwo.TabStop = false;
            this.GrbTwo.Text = "2. Änderungen";
            // 
            // LblEpisodeCountCounter
            // 
            this.LblEpisodeCountCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblEpisodeCountCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEpisodeCountCounter.Location = new System.Drawing.Point(621, 186);
            this.LblEpisodeCountCounter.Name = "LblEpisodeCountCounter";
            this.LblEpisodeCountCounter.Size = new System.Drawing.Size(43, 16);
            this.LblEpisodeCountCounter.TabIndex = 20;
            this.LblEpisodeCountCounter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEpisodeCount
            // 
            this.LblEpisodeCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblEpisodeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEpisodeCount.Location = new System.Drawing.Point(523, 186);
            this.LblEpisodeCount.Name = "LblEpisodeCount";
            this.LblEpisodeCount.Size = new System.Drawing.Size(92, 16);
            this.LblEpisodeCount.TabIndex = 19;
            // 
            // LblLoadMetadataInfo
            // 
            this.LblLoadMetadataInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblLoadMetadataInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLoadMetadataInfo.Location = new System.Drawing.Point(677, 205);
            this.LblLoadMetadataInfo.Name = "LblLoadMetadataInfo";
            this.LblLoadMetadataInfo.Size = new System.Drawing.Size(223, 94);
            this.LblLoadMetadataInfo.TabIndex = 18;
            // 
            // BtnGetMetadata
            // 
            this.BtnGetMetadata.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnGetMetadata.Enabled = false;
            this.BtnGetMetadata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGetMetadata.Location = new System.Drawing.Point(676, 155);
            this.BtnGetMetadata.Name = "BtnGetMetadata";
            this.BtnGetMetadata.Size = new System.Drawing.Size(224, 23);
            this.BtnGetMetadata.TabIndex = 17;
            this.BtnGetMetadata.Text = "Lade Metadaten";
            this.BtnGetMetadata.UseVisualStyleBackColor = true;
            this.BtnGetMetadata.Click += new System.EventHandler(this.BtnGetMetadata_Click);
            // 
            // BtnClearSearch
            // 
            this.BtnClearSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnClearSearch.Enabled = false;
            this.BtnClearSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnClearSearch.Location = new System.Drawing.Point(791, 87);
            this.BtnClearSearch.Name = "BtnClearSearch";
            this.BtnClearSearch.Size = new System.Drawing.Size(111, 23);
            this.BtnClearSearch.TabIndex = 16;
            this.BtnClearSearch.Text = "Suche löschen";
            this.BtnClearSearch.UseVisualStyleBackColor = true;
            this.BtnClearSearch.Click += new System.EventHandler(this.BtnClearSearch_Click);
            // 
            // CobOrder
            // 
            this.CobOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CobOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobOrder.Enabled = false;
            this.CobOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CobOrder.FormattingEnabled = true;
            this.CobOrder.Location = new System.Drawing.Point(717, 128);
            this.CobOrder.Name = "CobOrder";
            this.CobOrder.Size = new System.Drawing.Size(183, 21);
            this.CobOrder.TabIndex = 12;
            // 
            // LblOrder
            // 
            this.LblOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblOrder.AutoSize = true;
            this.LblOrder.Location = new System.Drawing.Point(678, 131);
            this.LblOrder.Name = "LblOrder";
            this.LblOrder.Size = new System.Drawing.Size(36, 13);
            this.LblOrder.TabIndex = 13;
            this.LblOrder.Text = "Order:";
            // 
            // LblEpisodeNames
            // 
            this.LblEpisodeNames.AutoSize = true;
            this.LblEpisodeNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEpisodeNames.Location = new System.Drawing.Point(230, 186);
            this.LblEpisodeNames.Name = "LblEpisodeNames";
            this.LblEpisodeNames.Size = new System.Drawing.Size(94, 16);
            this.LblEpisodeNames.TabIndex = 15;
            this.LblEpisodeNames.Text = "Folgennamen:";
            // 
            // LbEpisodeNames
            // 
            this.LbEpisodeNames.AllowDrop = true;
            this.LbEpisodeNames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LbEpisodeNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbEpisodeNames.FormattingEnabled = true;
            this.LbEpisodeNames.HorizontalScrollbar = true;
            this.LbEpisodeNames.ItemHeight = 15;
            this.LbEpisodeNames.Location = new System.Drawing.Point(233, 205);
            this.LbEpisodeNames.Name = "LbEpisodeNames";
            this.LbEpisodeNames.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.LbEpisodeNames.Size = new System.Drawing.Size(437, 94);
            this.LbEpisodeNames.TabIndex = 14;
            this.LbEpisodeNames.TabStop = false;
            // 
            // PbxThumbnail
            // 
            this.PbxThumbnail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PbxThumbnail.Location = new System.Drawing.Point(51, 121);
            this.PbxThumbnail.Name = "PbxThumbnail";
            this.PbxThumbnail.Size = new System.Drawing.Size(129, 181);
            this.PbxThumbnail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbxThumbnail.TabIndex = 11;
            this.PbxThumbnail.TabStop = false;
            this.PbxThumbnail.DoubleClick += new System.EventHandler(this.PbxThumbnail_DoubleClick);
            // 
            // BtnSearchOnTheTVDB
            // 
            this.BtnSearchOnTheTVDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSearchOnTheTVDB.Enabled = false;
            this.BtnSearchOnTheTVDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSearchOnTheTVDB.Location = new System.Drawing.Point(677, 87);
            this.BtnSearchOnTheTVDB.Name = "BtnSearchOnTheTVDB";
            this.BtnSearchOnTheTVDB.Size = new System.Drawing.Size(111, 23);
            this.BtnSearchOnTheTVDB.TabIndex = 10;
            this.BtnSearchOnTheTVDB.Text = "Serie suchen";
            this.BtnSearchOnTheTVDB.UseVisualStyleBackColor = true;
            this.BtnSearchOnTheTVDB.Click += new System.EventHandler(this.BtnSearchOnTheTVDB_Click);
            // 
            // LblFetchFromTheTVDB
            // 
            this.LblFetchFromTheTVDB.AutoSize = true;
            this.LblFetchFromTheTVDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFetchFromTheTVDB.Location = new System.Drawing.Point(230, 68);
            this.LblFetchFromTheTVDB.Name = "LblFetchFromTheTVDB";
            this.LblFetchFromTheTVDB.Size = new System.Drawing.Size(217, 16);
            this.LblFetchFromTheTVDB.TabIndex = 8;
            this.LblFetchFromTheTVDB.Text = "Serieneinträge von TheTVDB.com:";
            // 
            // LbSeriesOnTheTVDB
            // 
            this.LbSeriesOnTheTVDB.AllowDrop = true;
            this.LbSeriesOnTheTVDB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LbSeriesOnTheTVDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbSeriesOnTheTVDB.FormattingEnabled = true;
            this.LbSeriesOnTheTVDB.HorizontalScrollbar = true;
            this.LbSeriesOnTheTVDB.ItemHeight = 15;
            this.LbSeriesOnTheTVDB.Location = new System.Drawing.Point(233, 87);
            this.LbSeriesOnTheTVDB.Name = "LbSeriesOnTheTVDB";
            this.LbSeriesOnTheTVDB.Size = new System.Drawing.Size(437, 94);
            this.LbSeriesOnTheTVDB.TabIndex = 3;
            this.LbSeriesOnTheTVDB.TabStop = false;
            this.LbSeriesOnTheTVDB.SelectedIndexChanged += new System.EventHandler(this.LbFetchFromTheTVDB_SelectedIndexChanged);
            this.LbSeriesOnTheTVDB.DoubleClick += new System.EventHandler(this.LbFetchFromTheTVDB_DoubleClick);
            // 
            // NumSeasonNumber
            // 
            this.NumSeasonNumber.Location = new System.Drawing.Point(20, 87);
            this.NumSeasonNumber.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.NumSeasonNumber.Name = "NumSeasonNumber";
            this.NumSeasonNumber.Size = new System.Drawing.Size(89, 20);
            this.NumSeasonNumber.TabIndex = 2;
            this.NumSeasonNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumSeasonNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumSeasonNumber.ValueChanged += new System.EventHandler(this.NumSeasonNumber_ValueChanged);
            // 
            // LblFirstEpisodeNumber
            // 
            this.LblFirstEpisodeNumber.AutoSize = true;
            this.LblFirstEpisodeNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFirstEpisodeNumber.Location = new System.Drawing.Point(123, 68);
            this.LblFirstEpisodeNumber.Name = "LblFirstEpisodeNumber";
            this.LblFirstEpisodeNumber.Size = new System.Drawing.Size(93, 16);
            this.LblFirstEpisodeNumber.TabIndex = 7;
            this.LblFirstEpisodeNumber.Text = "Erste Episode";
            // 
            // NumFirstEpisodeNumber
            // 
            this.NumFirstEpisodeNumber.Location = new System.Drawing.Point(124, 87);
            this.NumFirstEpisodeNumber.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.NumFirstEpisodeNumber.Name = "NumFirstEpisodeNumber";
            this.NumFirstEpisodeNumber.Size = new System.Drawing.Size(89, 20);
            this.NumFirstEpisodeNumber.TabIndex = 3;
            this.NumFirstEpisodeNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumFirstEpisodeNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // TbSeriesName
            // 
            this.TbSeriesName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbSeriesName.Location = new System.Drawing.Point(6, 35);
            this.TbSeriesName.Name = "TbSeriesName";
            this.TbSeriesName.Size = new System.Drawing.Size(898, 20);
            this.TbSeriesName.TabIndex = 1;
            this.TbSeriesName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TbSeriesName.WaterMark = "z.B.: Die Simpsons";
            this.TbSeriesName.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.TbSeriesName.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbSeriesName.WaterMarkForeColor = System.Drawing.Color.LightGray;
            this.TbSeriesName.TextChanged += new System.EventHandler(this.TbSeriesName_TextChanged);
            // 
            // LblSeasonNumber
            // 
            this.LblSeasonNumber.AutoSize = true;
            this.LblSeasonNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSeasonNumber.Location = new System.Drawing.Point(30, 68);
            this.LblSeasonNumber.Name = "LblSeasonNumber";
            this.LblSeasonNumber.Size = new System.Drawing.Size(69, 16);
            this.LblSeasonNumber.TabIndex = 2;
            this.LblSeasonNumber.Text = "Staffelzahl";
            // 
            // LblSeriesName
            // 
            this.LblSeriesName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblSeriesName.AutoSize = true;
            this.LblSeriesName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSeriesName.Location = new System.Drawing.Point(415, 14);
            this.LblSeriesName.Name = "LblSeriesName";
            this.LblSeriesName.Size = new System.Drawing.Size(81, 16);
            this.LblSeriesName.TabIndex = 0;
            this.LblSeriesName.Text = "Serienname";
            // 
            // GrbThree
            // 
            this.GrbThree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrbThree.Controls.Add(this.BtnStart);
            this.GrbThree.Location = new System.Drawing.Point(12, 587);
            this.GrbThree.Name = "GrbThree";
            this.GrbThree.Size = new System.Drawing.Size(910, 65);
            this.GrbThree.TabIndex = 4;
            this.GrbThree.TabStop = false;
            this.GrbThree.Text = "3. Umbenennen";
            // 
            // BtnStart
            // 
            this.BtnStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStart.Location = new System.Drawing.Point(352, 19);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(203, 34);
            this.BtnStart.TabIndex = 4;
            this.BtnStart.Text = "Umbenennen starten";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // MstMain
            // 
            this.MstMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemFile,
            this.MstItemFolder,
            this.MstItemOptions,
            this.MstItemHelp});
            this.MstMain.Location = new System.Drawing.Point(0, 0);
            this.MstMain.Name = "MstMain";
            this.MstMain.Size = new System.Drawing.Size(934, 24);
            this.MstMain.TabIndex = 5;
            this.MstMain.Text = "menuStrip1";
            // 
            // MstItemFile
            // 
            this.MstItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemFileSelection});
            this.MstItemFile.Name = "MstItemFile";
            this.MstItemFile.Size = new System.Drawing.Size(46, 20);
            this.MstItemFile.Text = "&Datei";
            // 
            // MstItemFileSelection
            // 
            this.MstItemFileSelection.Name = "MstItemFileSelection";
            this.MstItemFileSelection.Size = new System.Drawing.Size(119, 22);
            this.MstItemFileSelection.Text = "&Auswahl";
            this.MstItemFileSelection.Click += new System.EventHandler(this.MstItemFileSelection_Click);
            // 
            // MstItemFolder
            // 
            this.MstItemFolder.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemFolderCreate});
            this.MstItemFolder.Name = "MstItemFolder";
            this.MstItemFolder.Size = new System.Drawing.Size(56, 20);
            this.MstItemFolder.Text = "&Ordner";
            // 
            // MstItemFolderCreate
            // 
            this.MstItemFolderCreate.Name = "MstItemFolderCreate";
            this.MstItemFolderCreate.Size = new System.Drawing.Size(118, 22);
            this.MstItemFolderCreate.Text = "&Erstellen";
            this.MstItemFolderCreate.Click += new System.EventHandler(this.MstItemFolderCreate_Click);
            // 
            // MstItemOptions
            // 
            this.MstItemOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemOptionsSettings});
            this.MstItemOptions.Name = "MstItemOptions";
            this.MstItemOptions.Size = new System.Drawing.Size(69, 20);
            this.MstItemOptions.Text = "&Optionen";
            // 
            // MstItemOptionsSettings
            // 
            this.MstItemOptionsSettings.Name = "MstItemOptionsSettings";
            this.MstItemOptionsSettings.Size = new System.Drawing.Size(145, 22);
            this.MstItemOptionsSettings.Text = "&Einstellungen";
            this.MstItemOptionsSettings.Click += new System.EventHandler(this.MstItemOptionsSettings_Click);
            // 
            // MstItemHelp
            // 
            this.MstItemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MstItemHelpAbout});
            this.MstItemHelp.Name = "MstItemHelp";
            this.MstItemHelp.Size = new System.Drawing.Size(44, 20);
            this.MstItemHelp.Text = "&Hilfe";
            // 
            // MstItemHelpAbout
            // 
            this.MstItemHelpAbout.Name = "MstItemHelpAbout";
            this.MstItemHelpAbout.Size = new System.Drawing.Size(107, 22);
            this.MstItemHelpAbout.Text = "&About";
            this.MstItemHelpAbout.Click += new System.EventHandler(this.MstItemHelpAbout_Click);
            // 
            // InitWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(934, 661);
            this.Controls.Add(this.GrbThree);
            this.Controls.Add(this.GrbTwo);
            this.Controls.Add(this.GrbOne);
            this.Controls.Add(this.MstMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MstMain;
            this.MinimumSize = new System.Drawing.Size(950, 700);
            this.Name = "InitWindow";
            this.Text = "AutoRenamer";
            this.GrbOne.ResumeLayout(false);
            this.GrbTwo.ResumeLayout(false);
            this.GrbTwo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxThumbnail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumSeasonNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumFirstEpisodeNumber)).EndInit();
            this.GrbThree.ResumeLayout(false);
            this.MstMain.ResumeLayout(false);
            this.MstMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LbSelection;
        private System.Windows.Forms.GroupBox GrbOne;
        private System.Windows.Forms.GroupBox GrbTwo;
        private System.Windows.Forms.Label LblSeasonNumber;
        private System.Windows.Forms.Label LblSeriesName;
        private System.Windows.Forms.GroupBox GrbThree;
        private Classes.DarkMode.DarkModeButton BtnStart;
        private KaisLib.Controls.WatermarkedTextBox TbSeriesName;
        private KaisLib.Controls.NumericUpDownLeadingZero NumFirstEpisodeNumber;
        private System.Windows.Forms.Label LblFirstEpisodeNumber;
        private KaisLib.Controls.NumericUpDownLeadingZero NumSeasonNumber;
        private System.Windows.Forms.Label LblFileCounter;
        private System.Windows.Forms.MenuStrip MstMain;
        private System.Windows.Forms.ToolStripMenuItem MstItemFile;
        private System.Windows.Forms.ToolStripMenuItem MstItemFolder;
        private System.Windows.Forms.ToolStripMenuItem MstItemHelp;
        private System.Windows.Forms.ToolStripMenuItem MstItemHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem MstItemFileSelection;
        private System.Windows.Forms.ToolStripMenuItem MstItemFolderCreate;
        private Classes.DarkMode.DarkModeButton BtnSearchOnTheTVDB;
        private System.Windows.Forms.Label LblFetchFromTheTVDB;
        private System.Windows.Forms.ListBox LbSeriesOnTheTVDB;
        private System.Windows.Forms.PictureBox PbxThumbnail;
        private System.Windows.Forms.Label LblOrder;
        private System.Windows.Forms.ComboBox CobOrder;
        private System.Windows.Forms.ListBox LbEpisodeNames;
        private System.Windows.Forms.Label LblEpisodeNames;
        private Classes.DarkMode.DarkModeButton BtnClearSearch;
        private Classes.DarkMode.DarkModeButton BtnGetMetadata;
        private System.Windows.Forms.ToolStripMenuItem MstItemOptions;
        private System.Windows.Forms.ToolStripMenuItem MstItemOptionsSettings;
        private System.Windows.Forms.Label LblLoadMetadataInfo;
        private System.Windows.Forms.Label LblEpisodeCountCounter;
        private System.Windows.Forms.Label LblEpisodeCount;
        private Classes.DarkMode.DarkModeButton BtnFileSelection;
    }
}

