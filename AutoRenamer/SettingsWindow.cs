﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using AutoRenamer.Classes.DarkMode;

/* Solution: AutoRenamer, SettingsWindow.cs
 * Settings Window
 * Kai Sackl, 22.10.2020
 */

namespace AutoRenamer
{
    public partial class SettingsWindow : Form
    {
        private bool initReady;

        public SettingsWindow()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            initReady = false;
            foreach (PropertyInfo language in typeof(LanguageCodes_ISO639_3).GetProperties())
            {
                string propValue = (string)language.GetValue(typeof(LanguageCodes_ISO639_3), null);
                string propName = language.Name;
                CobMetadataFirstLanguage.Items.Add(propName);
                CobMetadataFallbackLanguage.Items.Add(propName);
                if (propValue == Globals.userSettings.MetadataFirstLanguage)
                    CobMetadataFirstLanguage.SelectedIndex = CobMetadataFirstLanguage.Items.IndexOf(propName);
                if (propValue == Globals.userSettings.MetadataFallbackLanguage)
                    CobMetadataFallbackLanguage.SelectedIndex = CobMetadataFallbackLanguage.Items.IndexOf(propName);
            }
            initReady = true;

            CbxAddEpisodenameToFilename.Checked = Globals.userSettings.AddEpisodenameToFilename;
            CbxAddEpisodenameToFileDetails.Checked = Globals.userSettings.AddEpisodenameToFileDetails;
            CbxUIDarkMode.Checked = Globals.userSettings.DarkMode;

            if (Globals.userSettings.DarkMode)
                Globals.ChangeToDarkMode(this, true);
            else
                Globals.ChangeToDarkMode(this, false);
        }

        #region ComboBox Events
        private void CobMetadataFirstLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (initReady)
            {
                foreach (PropertyInfo language in typeof(LanguageCodes_ISO639_3).GetProperties())
                {
                    string propValue = (string)language.GetValue(typeof(LanguageCodes_ISO639_3), null);
                    string propName = language.Name;
                    if (propName == (string)CobMetadataFirstLanguage.SelectedItem)
                        Globals.userSettings.MetadataFirstLanguage = propValue;
                }
            }
        }

        private void CobMetadataFallbackLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (initReady)
            {
                foreach (PropertyInfo language in typeof(LanguageCodes_ISO639_3).GetProperties())
                {
                    string propValue = (string)language.GetValue(typeof(LanguageCodes_ISO639_3), null);
                    string propName = language.Name;
                    if (propName == (string)CobMetadataFallbackLanguage.SelectedItem)
                        Globals.userSettings.MetadataFallbackLanguage = propValue;
                }
            }
        }
        #endregion

        #region Button Events
        private void BtnSettingsSaveSettings_Click(object sender, EventArgs e)
        {
            try { Globals.WriteSettingsToFile(Globals.settingsFilePath); }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error!"); }
        }
        #endregion

        #region Checkbox Events
        private void CbxAddEpisodenameToFilename_CheckedChanged(object sender, EventArgs e)
            => Globals.userSettings.AddEpisodenameToFilename = CbxAddEpisodenameToFilename.Checked;

        private void CbxAddEpisodenameToFileDetails_CheckedChanged(object sender, EventArgs e) 
            => Globals.userSettings.AddEpisodenameToFileDetails = CbxAddEpisodenameToFileDetails.Checked;

        private void CbxUIDarkMode_CheckedChanged(object sender, EventArgs e)
        {
            Globals.userSettings.DarkMode = CbxUIDarkMode.Checked;
            foreach (Form form in Application.OpenForms)
            {
                if (CbxUIDarkMode.Checked)
                    Globals.ChangeToDarkMode(form, true);
                else
                    Globals.ChangeToDarkMode(form, false);
            } 
        }
        #endregion

        private void TbcSettings_DrawItem(object sender, DrawItemEventArgs e)
        {
            // Redraw control for dark mode
            TabPage page = TbcSettings.TabPages[e.Index];
            Rectangle bounds = e.Bounds;
            int yOffset = (e.State == DrawItemState.Selected) ? -2 : 1;
            bounds.Offset(1, yOffset);

            if (Globals.userSettings.DarkMode)
            {
                e.Graphics.FillRectangle(new SolidBrush(DarkModeColors.LightBlack), e.Bounds);
                TextRenderer.DrawText(e.Graphics, page.Text, e.Font, bounds, DarkModeColors.LightGray);
            }
            else
            {
                e.Graphics.FillRectangle(new SolidBrush(default), e.Bounds);
                TextRenderer.DrawText(e.Graphics, page.Text, e.Font, bounds, default);
            }
        }

        // Enable Double-Buffer for all controls of the form
        // "Fixes" Combobox flickering
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
    }
}
