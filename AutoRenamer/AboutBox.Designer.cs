﻿namespace AutoRenamer
{
    partial class AboutBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox));
            this.okButton = new System.Windows.Forms.Button();
            this.LblCopyright = new System.Windows.Forms.Label();
            this.LblVersion = new System.Windows.Forms.Label();
            this.LblProductName = new System.Windows.Forms.Label();
            this.LogPictureBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.TbDescription = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.LogPictureBox)).BeginInit();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(339, 239);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "&OK";
            // 
            // LblCopyright
            // 
            this.LblCopyright.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblCopyright.Location = new System.Drawing.Point(143, 52);
            this.LblCopyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.LblCopyright.MaximumSize = new System.Drawing.Size(0, 17);
            this.LblCopyright.Name = "LblCopyright";
            this.LblCopyright.Size = new System.Drawing.Size(271, 17);
            this.LblCopyright.TabIndex = 21;
            this.LblCopyright.Text = "Copyright";
            this.LblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LblVersion
            // 
            this.LblVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblVersion.Location = new System.Drawing.Point(143, 26);
            this.LblVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.LblVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.LblVersion.Name = "LblVersion";
            this.LblVersion.Size = new System.Drawing.Size(271, 17);
            this.LblVersion.TabIndex = 0;
            this.LblVersion.Text = "Version";
            this.LblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LblProductName
            // 
            this.LblProductName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblProductName.Location = new System.Drawing.Point(143, 0);
            this.LblProductName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.LblProductName.MaximumSize = new System.Drawing.Size(0, 17);
            this.LblProductName.Name = "LblProductName";
            this.LblProductName.Size = new System.Drawing.Size(271, 17);
            this.LblProductName.TabIndex = 19;
            this.LblProductName.Text = "Product Name";
            this.LblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LogPictureBox
            // 
            this.LogPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("LogPictureBox.Image")));
            this.LogPictureBox.Location = new System.Drawing.Point(3, 3);
            this.LogPictureBox.Name = "LogPictureBox";
            this.tableLayoutPanel.SetRowSpan(this.LogPictureBox, 6);
            this.LogPictureBox.Size = new System.Drawing.Size(131, 131);
            this.LogPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogPictureBox.TabIndex = 12;
            this.LogPictureBox.TabStop = false;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.LogPictureBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.LblProductName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.LblVersion, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.LblCopyright, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.TbDescription, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.okButton, 1, 5);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(417, 265);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // TbDescription
            // 
            this.TbDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbDescription.Location = new System.Drawing.Point(143, 107);
            this.TbDescription.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.TbDescription.Multiline = true;
            this.TbDescription.Name = "TbDescription";
            this.TbDescription.ReadOnly = true;
            this.TbDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TbDescription.Size = new System.Drawing.Size(271, 126);
            this.TbDescription.TabIndex = 23;
            this.TbDescription.TabStop = false;
            this.TbDescription.Text = "Description";
            // 
            // AboutBox
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 283);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBox";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AboutBox";
            ((System.ComponentModel.ISupportInitialize)(this.LogPictureBox)).EndInit();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label LblCopyright;
        private System.Windows.Forms.Label LblVersion;
        private System.Windows.Forms.Label LblProductName;
        private System.Windows.Forms.PictureBox LogPictureBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox TbDescription;
    }
}
