## [1.4.2](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.4.2) (2021-04-05)
### Changes
- Bump .NET Framework version to v4.7.2

### Bug Fixes
- Fix fetching episode names from thetvdb.com, since TheTVDB website changes breaked fetching

## [1.4.1](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.4.1) (2020-11-22)
### Bug Fixes
- Fix null reference error on fallback language, if fallback translated episode title is null

# [1.4.0](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.4.0) (2020-11-14)
### New Features
- Main Window and CreateFolder Window are now resizable
- New settings menu
- Search and fetch episodenames for a series directly in the program from thetvdb.com with your prefered language (and a fallback language, if no episode information with prefered language is available)
- Option to append fetched episodenames to filename
- Option to add episodename to file details (experimental!)
- Dark Mode

### Changes
- AutoRenamer_Data folder is gone

### Bug Fixes
- Program crashed, if new filename was too long

# [1.3.0](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.3.0) (2020-02-04)
### New Features
- Series name and season number will be set automatically, if files are already in the correct folder (eg.: ...\SeriesName\Season XX\example.mp4)

### Changes
- Sort order should now be really like the windows version (Program now imports a windows system library for comparing/sorting. Minimun windows version now is: Win XP or Win Server 2003)
- Whitespaces in front and end of series name will be trimmed

# [1.2.0](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.2.0) (2019-03-12)
### New Features
- Folder creator (Create batch of (season) folders)
- File Counter (Counts how many files are selected)
- Menu Strip

### Changes
- Change sort order back to alphabetic sort order (NumericComparer.dll removed)

## [1.1.1](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.1.1) (2018-12-30)
### Bug Fixes
- Fix bug where season and episode number couldn't be 00 (special seasons and episodes)

# [1.1.0](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.1.0) (2018-12-30)
### New Features
- New Control to choose number of first episode to rename

### Changes
- Change alphabetic sort order to natural numeric sort order (like Windows Explorer)
- Change season number textbox to numericUpDown control

## [1.0.1](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.0.1) (2018-12-07)
### Bug Fixes
- Fixed crash when new filename contains invalid characters

# [1.0.0](https://gitlab.com/cannonball1911/AutoRenamer/tags/v1.0.0) (2018-08-19)
### First release
